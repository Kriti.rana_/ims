<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Supplier;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $categories = Category::select('id')->get()->pluck('id')->toArray();
        $suppliers = Supplier::select('id')->get()->pluck('id')->toArray();
        $price = mt_rand(100, 10000);

        return [
            'name' => $this->faker->words(3, true),
            'category_id' => $categories[mt_rand(0, count($categories) - 1)],
            'supplier_id' => $suppliers[mt_rand(0, count($suppliers) - 1)],
            'code' => Str::random(4),
            'buying_price' => $price,
            'selling_price' => $price + mt_rand(10, 150),
            'buying_date' => now()->subDays(mt_rand(1, 5))->format('Y-m-d'),
        ];
    }
}

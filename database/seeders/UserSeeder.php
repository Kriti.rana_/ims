<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Supplier;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('password'),
            'role' => 'Admin',
        ]);


        $supplier_user = User::create([
            'name' => 'Supplier',
            'email' => 'supplier@supplier.com',
            'password' => bcrypt('supplier'),
            'role' => 'Supplier',
        ]);

        Supplier::create([
            'name' => 'Supplier',
            'user_id' => $supplier_user->id,
            'address' => 'Addr',
            'phone' => 9865344517,
            'shop_name' => 'ABC Shop',
        ]);

        User::create([
            'name' => 'Cashier',
            'email' => 'pos@pos.com',
            'password' => bcrypt('password'),
            'role' => 'Cashier',
        ]);
    }
}

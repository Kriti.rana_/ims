<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [];
        $now = now();
        $categories_list = ['Laptop', 'Desktop', 'Mobile'];
        foreach ($categories_list as $cat) {
            $categories[] = [
                'name' => $cat,
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }

        Category::insert($categories);
    }
}

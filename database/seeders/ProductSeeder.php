<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\Stock;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::factory(30)->create();
        $now = now();
        $stock_data = [];
        foreach ($products as $product) {
            $stock_data[] = [
                'product_id' => $product->id,
                'quantity' => mt_rand(50, 300),
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }

        Stock::insert($stock_data);
    }
}

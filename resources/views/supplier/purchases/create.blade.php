@extends('supplier.layout')
@section('title', 'Add Product Supply')

@section('content')

    <x-alert />

    <div class="card">
        <div class="card-header">
            <h3 class="card-title" style="font-size:1.3rem;line-height:1.8;
            font-weight:bold">
                Add New Product Supply</h3>
            <div class="card-tools">
                <a class="btn btn-outline-secondary" href="{{ route('supplier.purchases.index') }}">
                    <i class="fas fa-arrow-circle-left mr-2"></i>
                    Go Back
                </a>
            </div>
        </div>

        <div class="card-body">
            <form action="{{ route('supplier.purchases.store') }}" method="post" enctype="multipart/form-data">
                @csrf

                <x-input type="select" field="product_id" text="Product" :options="$products" />

                <x-input type="number" field="quantity" text="Quantity" />

                <x-input type="number" field="price" text="Unit Price" />

                <button class="btn btn-outline-secondary">
                    <i class="fas fa-save mr-2"></i>
                    Save
                </button>
            </form>
        </div>
    </div>

@endsection

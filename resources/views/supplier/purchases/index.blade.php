@extends('supplier.layout')
@section('title', 'Supplied Products')

@section('content')

    <x-alert />

    <div class="card">
        <div class="card-header">
            <h3 class="card-title" style="font-size:1.3rem;line-height:1.8;
                    font-weight:bold">
                Supplied Products
            </h3>
            <div class="card-tools">
                <a class="btn btn-outline-secondary" href="{{ route('supplier.purchases.create') }}">
                    <i class="fas fa-plus-circle mr-2"></i>
                    Add New
                </a>
            </div>
        </div>

        <div class="card-body p-0">
            <table class="table table-bordered">
                <thead class="bg-secondary">
                    <tr>
                        <th>ID</th>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Total</th>
                        <th>Date</th>
                        <th>Approved</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($purchases as $purchase)
                        <tr>
                            <td>{{ $purchase->id }}</td>
                            <td>{{ $purchase->product->name }}</td>
                            <td>Rs. {{ $purchase->price }}</td>
                            <td>{{ $purchase->quantity }}</td>
                            <td>Rs. {{ $purchase->total }}</td>
                            <td>{{ $purchase->created_at }}</td>
                            <td>{{ $purchase->approved_at ?? 'No' }}</td>
                            <td>
                                <a class="btn btn-outline-secondary btn-sm"
                                    href="{{ route('supplier.purchases.show', $purchase) }}">
                                    <i class="fas fa-eye mr-2"></i>
                                    Show
                                </a>

                                {{-- <a class="btn btn-primary btn-sm" href="{{route('admin.purchases.edit',$purchase)}}">
                            <i class="fas fa-edit mr-2"></i>
                            Edit
                        </a> --}}

                                <form class="d-inline" action="{{ route('supplier.purchases.destroy', $purchase) }}"
                                    method="post">
                                    @csrf
                                    @method('DELETE')

                                    <button class="btn btn-outline-danger btn-sm">
                                        <i class="fas fa-trash mr-2"></i>
                                        Delete
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @if (count($purchases) == 0)
                <div class="alert alert-warning text-center mb-0">
                    No Items Found!
                </div>
            @endif
            <div class="card-fotter">
                {{ $purchases->links() }}
            </div>
        </div>
    </div>

@endsection

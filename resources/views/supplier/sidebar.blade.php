<li class="nav-item">
    <a class="nav-link {{ request()->routeIs('/supplier') ? 'active' : '' }} " href="{{ url('/supplier') }}">
        <i class="nav-icon fas fa-truck-loading"></i>

        <p>Supplier</p>
    </a>
</li>

<li class="nav-item">
    <a class="nav-link {{ request()->routeIs('/supplier/products') ? 'active' : '' }} "
        href="{{ url('/supplier/products') }}">
        <i class="nav-icon fas fa-fw fa-truck"></i>

        <p>Products</p>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link {{ request()->routeIs('/supplier/purchases') ? 'active' : '' }} "
        href="{{ url('/supplier/purchases') }}">
        <i class="nav-icon fas fa-fw fa-clipboard-list"></i>

        <p>Supply Products</p>
    </a>
</li>


{{-- <li class="nav-item">
    <a class="nav-link {{ request()->routeIs('') ? 'active' : '' }} "
      href="{{ url('') }}">
      <i class="nav-icon far fa-calendar-check"></i>

      <p>Profile</p>
    </a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="#">
      <i class="nav-icon fas fa-envelope-open-text"></i>

      <p>Mail</p>
    </a>
  </li> --> --}}

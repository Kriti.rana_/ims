@extends('supplier.layout')

@section('title', 'Products')

@section('content')

<x-alert />

<form action="{{ route('supplier.products.search')}}" method="GET" role="search">

    <div class="input-group mb-4">
        <input type="text"  name="q" class="form-control" placeholder="Type Name..">
        <div class="input-group-append">
          <button class="btn btn-outline-secondary" type="submit">
            <span class="fas fa-search"></span>
          </button>
        </div>
      </div>

</form>

<div class="card">
    <div class="card-header">

        <h3 class="card-title" style="font-size: 1.5rem;line-height:1.8;font-weight:bold">
            All Products 
        </h3>

        <div class="card-tools">

            


            <a class="btn btn-outline-secondary" href="{{ route('supplier.products.create') }}"> 
                <i class="fas fa-plus-circle mr-2"></i>Add New</a>
        </div>

    </div>

      <div class="card-body p-0">
          <table class="table table-bordered table-striped">
              <thead class="bg-secondary">
                  <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Category</th>
                      <th>Quantity</th>
                      <th>Product Image</th>
                      <th> Action</th>
                     
                  </tr>
              </thead>

              <tbody>
                  @foreach ($products as $product)
                  <tr>
                      <td>{{ $product->id }}</td>
                      <td>{{ $product->name }}</td>
                      <td>{{ $product->category->name }}</td>
                      <td>{{ $product->stocks_sum_quantity ?? 0 }}</td>
                      
                      <td>
                          @if ($product->media)
                              <img src="/storage/{{ $product->media->path }}" height="70px" />
                          @endif
                      </td>
                      
                      <td>
                          <a class="btn btn-outline-secondary btn-sm" href="{{ route('supplier.products.show', $product) }}">
                           
                            <i class="fas fa-eye mr-2"></i>
                            Details

                           </a>

                           <a class="btn btn-outline-secondary btn-sm" href="{{ route('supplier.products.variants', $product) }}">
                            <i class="fas fa-layer-group mr-2"></i>
                            Variants
                           </a>

                           <a class="btn btn-outline-secondary btn-sm" href="{{ route('supplier.products.edit', $product) }}">
                           
                            <i class="fas fa-edit mr-2"></i>
                            Edit

                           </a>
                           

                          
                      </td>
                  </tr>
                      
                  @endforeach
              </tbody>
          </table>
          @if(count($products) == 0)
          <div class="alert alert-warning text-center mb-0">
              No Items Found!
          </div>
          @endif
      </div>

      <div class="card-footer">
        {{ $products->links() }}
      </div>
         

      </div>

</div>

@endsection
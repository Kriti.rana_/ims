@extends('supplier.layout')
@section('title','Product Variants')

@section('css')
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css') }}">
@endsection

@section('js')
<script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
<script>
$(document).ready(function() {
    $('#product_id').select2();
});
</script>
@endsection

@section('content')

<x-alert />

<div class="card">
    <div class="card-header">
        <h3 class="card-title" style="font-size:1.3rem;line-height:1.8;font-weight:bold">
            Product Variants: {{ $product->name }}
        </h3>
        <div class="card-tools">
            <a class="btn btn-outline-secondary" href="{{ route('supplier.products.index') }}">
            <i class="fas fa-arrow-circle-left mr-2"></i>
                Go Back
            </a>
        </div>
    </div>

    <div class="card-body">
        <form action="{{ route('supplier.products.variants', $product) }}" method="post">
            @csrf

            <div class="row">
                <div class="col-md-10">
                    <x-input
                        type="select"
                        field="product_id"
                        text="Product"
                        :options="$all_products"
                    />
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="">&nbsp;</label><br>
                        <button type="submit" class="btn btn-primary">
                            Add Variant
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="card-body p-0">
        <table class="table table-bordered">
            <thead class="bg-primary">
                <tr>
                    <th style="width: 100px">S.N</th>
                    <th>Variant</th>
                    <th>Price</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse($products as $pro)
                <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $pro->product_id == $product->id ? $pro->variant?->name : $pro->product?->name }}</td>
                    <td>Rs. {{ $pro->product_id == $product->id ? $pro->variant?->selling_price : $pro->product?->selling_price  }}</td>
                    <td>
                        <form action="{{ route('supplier.products.variants.delete', [$product, $pro]) }}" method="post">
                            @method('DELETE')
                            @csrf
                            <button class="btn btn-danger btn-sm">
                                <i class="fas fa-fw fa-trash mr-2"></i>
                                <span>Delete</span>
                            </button>
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="3" style="text-align: center">
                       There are no variants added for this product!
                    </td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>

<style>
    .block-margin .form-group {
        margin-bottom: 0!important;
    }
</style>
@endsection
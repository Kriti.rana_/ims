@extends('Supplier.layout')

@section('title', 'Product Details')

@section('content')

<x-alert />

<div class="card">
    <div class="card-header">

        <h3 class="card-title" style="font-size: 1.5rem;line-height:1.8;font-weight:bold">
            Product Details
        </h3>

        <div class="card-tools">
            <a class="btn btn-outline-secondary" href="{{ route('supplier.products.index') }}"> 
                <i class="fas fa-arrow-circle-left mr-2"></i>
                Go Back
            </a>
        </div>

    </div>

      <div class="row card-body p-0">
          <div class="col">
          <table class="table table-bordered table-striped">
              <tr>
                  <th style="width: 50%">ID</th>
                  <td>{{ $product->id }}</td>

              </tr>
              <tr>
                <th>Name</th>
                <td>{{ $product->name }}</td>
                
            </tr>
            <tr>
                <th>Category</th>
                <td>{{ $product->category->name }}</td>
                
            </tr>
            <tr>
                <th>Supplier</th>
                <td>{{ $product->supplier->name }}</td>
                
            </tr>
            <tr>
                <th>Code</th>
                <td>{{ $product->code }}</td>
                
            </tr>
            <tr>
                <th>Unit Price</th>
                <td>{{ $product->buying_price }}</td>
                
            </tr>
            <tr>
                <th>Unit Selling Price</th>
                <td>{{ $product->selling_price }}</td>
                
            </tr>
           
            <tr>
                <th>Quantity</th>
                <td>{{ $product->quantity }}</td>
                
            </tr>
            <tr>
                <th>Buying Date</th>
                <td>{{ $product->buying_date }}</td>
                
            </tr>
            
            <tr>
                <th>Created</th>
                <td>{{ $product->created_at }}</td>
                
            </tr>
            <tr>
                <th>Updated</th>
                <td>{{ $product->updated_at }}</td>
                
            </tr>
          </table>
          
      </div>

      <div class="col">
          <h1 style="text-align: center">{{ $product->name }}</h1>
             @if ($product->media)
                <img src="/storage/{{ $product->media->path }}" height="500px" />
            @endif

      </div>
     
      
      </div>

</div>

@endsection
@extends('supplier.layout')

@section('title', 'Supplier Dashboard')

@section('content')

<div class="card">
    <div class="card-body">
       
        <div class="row">
            <div class="col-lg-3 col-6">
            
            <div class="small-box bg-secondary">
            <div class="inner">
            <h3>{{ $count['product'] }} </h3>
            <p>Products</p>
            </div>
            <div class="icon">
            <i class="ion ion-bag"></i>
            </div>
            <a href="/supplier/products" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
            </div>
            
        </div>
    </div>
</div>

@endsection

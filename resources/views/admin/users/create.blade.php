@extends('adminlte::page')

@section('title', 'Add New User')
    

@section('content')
<x-alert />

<div class="card">
    <div class="card-header">
        <h3 class="card-title" style="font-size: 1.5rem;line-height: 1.8;
        font-weight:bold">
            Add New User
        </h3>
        <div class="card-tools">
            <a class="btn btn-outline-secondary" href="{{ route('admin.users.index') }}">
                <i class="fas fa-arrow-circle-left mr-2"></i>
                Go Back
            </a>
        </div>
    </div>

    <div class="card-body">

        <form action="{{ route('admin.users.store') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" 
                value="{{ old('name') }}">
                @error('name')
                <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>
            
            <div class="form-group">
                <label for="email">Email Adddress</label>
                <input type="email" name="email" id="email"
                class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}">
                @error('email')
                <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>

            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" id="password"
                class="form-control @error('password') is-invalid @enderror" >
                @error('password')
                <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>

            <div class="form-group">
                <label for="role">Role </label>
                <select name="role" id="role" class="form-control @error('role') is-invalid @enderror">
                    @foreach(\App\Models\User::CRUD_ROLES as $role)
                    <option value="{{ $role }}" @if(old('role')==$role) selected @endif> 
                        {{ $role }} 
                    </option>
                    @endforeach
                </select>
                @error('role')
                <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>

            <button class="btn btn-outline-secondary">
                <i class="fas fa-save mr-2"></i>
                Save
            </button> 

        </form>
    
    </div>
</div>
    
@endsection
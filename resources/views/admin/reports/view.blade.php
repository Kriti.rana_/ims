@extends('adminlte::page')

@section('title', 'Report')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Report from {{ $start_date->format('Y-m-d') }} to {{ $end_date->format('Y-m-d') }}</h3>
        <div class="card-tools">
            <a href="{{ route('admin.reports.index') }}" class="d-print-none btn btn-outline-secondary">
                Go Back
            </a>
        </div>
    </div>
    <div class="card-body p-0">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>SN</th>
                    <th>Item</th>
                    <th>Type</th>
                    <th>Amount</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
                @foreach($reports as $report)
                <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $report['title'] }}</td>
                    <td>{{ $report['type'] }}</td>
                    <td>Rs. {{ $report['amount'] }}</td>
                    <td>{{ date("Y-m-d", $report['time']) }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
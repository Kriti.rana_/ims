@extends('adminlte::page')

@section('title', 'Generate Report')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Generate Report</h3>
    </div>
    <div class="card-body">
        <form action="{{ route('admin.reports.view') }}" method="get">
        
        <x-input type="date" field="start_date" text="Start Date" />
        <x-input type="date" field="end_date" text="End Date" />
        
        <button class="btn btn-primary">
            Generate Report
        </button>
        
        </form>
    </div>
</div>
@endsection
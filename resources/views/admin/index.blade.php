@extends('adminlte::page')

@section('content')

<div class="card">
    <div class="card-body">
       
        <div class="row">
            <div class="col-lg-3 col-6">
            
            <div class="small-box bg-secondary">
            <div class="inner">
            <h3>{{ $count['category'] }} </h3>
            <p>Categories</p>
            </div>
            <div class="icon">
            <i class="ion ion-bag"></i>
            </div>
            <a href="/admin/categories" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
            </div>
            
            <div class="col-lg-3 col-6">
            
            <div class="small-box bg-secondary">
            <div class="inner">
            <h3>{{ $count['customer'] }}</h3>
            <p>Customers</p>
            </div>
            <div class="icon">
            <i class="ion ion-stats-bars"></i>
            </div>
            <a href="/admin/customers" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
            </div>
            
            <div class="col-lg-3 col-6">
            
            <div class="small-box bg-secondary">
            <div class="inner">
            <h3>{{ $count['supplier'] }}</h3>
            <p>Suppliers</p>
            </div>
            <div class="icon">
            <i class="ion ion-person-add"></i>
            </div>
            <a href="/admin/suppliers" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
            </div>
            
            <div class="col-lg-3 col-6">
            
            <div class="small-box bg-secondary">
            <div class="inner">
            <h3>{{ $count['product'] }}</h3>
            <p>Products</p>
            </div>
            <div class="icon">
            <i class="ion ion-pie-graph"></i>
            </div>
            <a href="/admin/products" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
            </div>
            
            </div>
    </div>
</div>

@endsection
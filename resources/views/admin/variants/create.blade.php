@extends('adminlte::page')

@section('title', 'Add New Variant')

@section('content')

<x-alert />

<div class="card">
    <div class="card-header">

        <h3 class="card-title" style="font-size: 1.5rem;line-height:1.8;font-weight:bold">
            Add New Variant
        </h3>

        <div class="card-tools">
            <a class="btn btn-outline-secondary" href="{{ route('admin.variants.index') }}"> <i class="fas fa-arrow-circle-left mr-2"></i>Go Back</a>
        </div>

    </div>

      <div class="card-body">
          <form action="{{ route('admin.variants.store') }}" method="post" enctype="multipart/form-data">
            @csrf

            <div class="form-group">
                <label for="product_id">Product</label>
                <select name="product_id" id="product_id" class="form-control @error('product_id')
                is-invalid
            @enderror">
            <option value="">Choose one...</option>
            @foreach ($products as $product)
            <option value="{{ $product->id }}">{{ $product->name }}</option>
            @endforeach

                </select>

            @error('product_id')
                    <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>

            <div class="form-group">
                <label for="name">Variant Name</label>
                <input type="text" name="name" id="name" class="form-control @error('name')
                is-invalid
            @enderror" value="{{ old('name') }}">

            @error('name')
                <small class="form-text text-danger">{{ $message }}</small>
            @enderror
            </div>

            <div class="form-group">
                <label for="quantity">Quantity</label>
                <input type="number" name="quantity" id="quantity" value="1" class="form-control @error('quantity')
                is-invalid
            @enderror" value="{{ old('quantity') }}">

                @error('quantity')
                    <small class="form-text text-danger">{{ $message }}</small>
                @enderror

            </div>

            <div class="form-group">
                <label for="price">Price</label>
                <input type="number" name="price" id="price" placeholder="0" class="form-control @error('price')
                is-invalid
            @enderror" value="{{ old('price') }}">

            @error('price')
                    <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>

            <div class="form-group">
                <label for="details">Details</label>
                <textarea name="details" id="details" class="form-control @error('details')
                is-invalid
            @enderror">{{ old('details') }}</textarea>

                @error('details')
                <small class="form-text text-danger">{{ $message }}</small>
            @enderror
            </div>

            <div class="form-group">
                <label for="image">Photo</label>
                <input type="file" name="image" id="image" class="form-control-file @error('image') is-invalid
                    
                @enderror">

                @error('image')
                    <small class="form-text text-danger">{{ $message }}</small>
                @enderror

            </div>

            <button class="btn btn-outline-secondary">
                <i class="fas fa-save mr-2"></i>
                Save
            </button>
          </form>
      </div>

</div>

@endsection
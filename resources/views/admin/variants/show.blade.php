@extends('adminlte::page')

@section('title', 'Variant Details')

@section('content')

<x-alert />

<div class="card">
    <div class="card-header">

        <h3 class="card-title" style="font-size: 1.5rem;line-height:1.8;font-weight:bold">
            Variant Details
        </h3>

        <div class="card-tools">
            <a class="btn btn-outline-secondary" href="{{ route('admin.variants.index') }}"> 
                <i class="fas fa-arrow-circle-left mr-2"></i>
                Go Back
            </a>
        </div>

    </div>

      <div class="row card-body p-0">
          <div class="col">
          <table class="table table-bordered table-striped">
              <tr>
                  <th style="width: 50%">ID</th>
                  <td>{{ $variant->id }}</td>

              </tr>
              <tr>
                <th>Product</th>
                <td>{{ $variant->product->name }}</td>
            </tr>
              <tr>
                <th>Variant Name</th>
                <td>{{ $variant->name }}</td>
            </tr>
            <tr>
                <th>Price</th>
                <td>{{ $variant->price }}</td>
                
            </tr>
            <tr>
                <th>Quantity</th>
                <td>{{ $variant->quantity }}</td>
                
            </tr>
            <tr>
                <th>Details</th>
                <td>{{ $variant->details }}</td>
                
            </tr>
            
            <tr>
                <th>Created</th>
                <td>{{ $variant->created_at }}</td>
                
            </tr>
            <tr>
                <th>Updated</th>
                <td>{{ $variant->updated_at }}</td>
                
            </tr>
          </table>
          
      </div>

      <div class="col">
          <h1 style="text-align: center">{{ $variant->name }}</h1>
             @if ($variant->media)
                <img src="/storage/{{ $variant->media->path }}" height="500px" />
            @endif

      </div>
     
      
      </div>

</div>

@endsection
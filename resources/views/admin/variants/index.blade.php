@extends('adminlte::page')

@section('title', 'All Variants')

@section('content')

<x-alert />

<div class="card">
    <div class="card-header">

        <h3 class="card-title" style="font-size: 1.5rem;line-height:1.8;font-weight:bold">
            All Variants
        </h3>

        <div class="card-tools">
            <a class="btn btn-outline-secondary" href="{{ route('admin.variants.create') }}"> <i class="fas fa-plus-circle mr-2"></i>Add New</a>
        </div>

    </div>

      <div class="card-body p-0">
          <table class="table table-bordered table-striped">
              <thead class="bg-secondary">
                  <tr>
                      <th>ID</th>
                      <th>Product</th>
                      <th>Variant Name</th>
                      <th>Price</th>
                      <th>Quantity</th>
                      <th>Details</th>
                      <th>Product Image</th>
                      <th>Date</th>
                      <th>Action</th>
                  </tr>
              </thead>

              <tbody>
                  @foreach ($variants as $variant)
                  <tr>
                      <td>{{ $variant->id }}</td>
                      <td>{{ $variant->product->name }}</td>
                      <td>{{ $variant->name }}</td>
                      <td>{{ $variant->price }}</td>
                      <td>{{ $variant->quantity }}</td>
                      <td>{{ $variant->details }}</td>
                      <td>
                        @if ($variant->media)
                            <img src="/storage/{{ $variant->media->path }}" height="70px" />
                        @endif
                    </td>
                      <td>{{ $variant->created_at }}</td>
                      <td>
                          <a class="btn btn-outline-secondary btn-sm" href="{{ route('admin.variants.show', $variant) }}">
                           
                            <i class="fas fa-eye mr-2"></i>
                            Details

                           </a>
                           <a class="btn btn-outline-secondary btn-sm" href="{{ route('admin.variants.edit', $variant) }}">
                           
                            <i class="fas fa-edit mr-2"></i>
                            Edit

                           </a>

                           
                      </td>
                  </tr>
                      
                  @endforeach
              </tbody>
          </table>
          <div class="card-footer">
            {{ $variants->links() }}
          </div>



      </div>

</div>

@endsection
@extends('adminlte::page')

@section('title', 'Customer Details')

@section('content')

<x-alert />

<div class="card">
    <div class="card-header">

        <h3 class="card-title" style="font-size: 1.5rem;line-height:1.8;font-weight:bold">
            Customer Details
        </h3>

        <div class="card-tools">
            <a class="btn btn-outline-secondary" href="{{ route('admin.customers.index') }}"> 
                <i class="fas fa-arrow-circle-left mr-2"></i>
                Go Back
            </a>
        </div>

    </div>

      <div class="card-body p-0">
          <table class="table table-bordered table-striped">
              <tr>
                  <th style="width: 20%">ID</th>
                  <td>{{ $customer->id }}</td>

              </tr>
              <tr>
                <th>Name</th>
                <td>{{ $customer->name }}</td>
                
            </tr>
            <tr>
                <th>Email</th>
                <td>{{ $customer->email }}</td>
                
            </tr>
            <tr>
                <th>Phone</th>
                <td>{{ $customer->phone }}</td>
                
            </tr>
            <tr>
                <th>Address</th>
                <td>{{ $customer->address }}</td>
                
            </tr>
           
            <tr>
                <th>Photo</th>
                <td> @if ($customer->media)
                    <img src="/storage/{{ $customer->media->path }}" height="70px" />
                @endif
                </td>
                
            </tr>
            <tr>
                <th>Created</th>
                <td>{{ $customer->created_at }}</td>
                
            </tr>
            <tr>
                <th>Updated</th>
                <td>{{ $customer->updated_at }}</td>
                
            </tr>
          </table>
          
      </div>

</div>

@endsection
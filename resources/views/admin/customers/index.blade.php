@extends('adminlte::page')

@section('title', 'All Customers')

@section('content')

<x-alert />

<div class="card">
    <div class="card-header">

        <h3 class="card-title" style="font-size: 1.5rem;line-height:1.8;font-weight:bold">
            All Customers
        </h3>

        <div class="card-tools">
            <a class="btn btn-outline-secondary" href="{{ route('admin.customers.create') }}"> <i class="fas fa-user-plus mr-2"></i>Add New</a>
        </div>

    </div>

      <div class="card-body p-0">
          <table class="table table-bordered table-striped">
              <thead class="bg-secondary">
                  <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Phone</th>
                      <th>Address</th>
                      <th>Photo</th>
                      <th>Date</th>
                      <th>Action</th>
                  </tr>
              </thead>

              <tbody>
                  @foreach ($customers as $customer)
                  <tr>
                      <td>{{ $customer->id }}</td>
                      <td>{{ $customer->name }}</td>
                      <td>{{ $customer->email }}</td>
                      <td>{{ $customer->phone }}</td>
                      <td>{{ $customer->address }}</td>
                      <td>
                          @if ($customer->media)
                              <img src="/storage/{{ $customer->media->path }}" height="70px" />
                          @endif
                      </td>
                      <td>{{ $customer->created_at }}</td>
                      <td>
                          <a class="btn btn-outline-secondary btn-sm" href="{{ route('admin.customers.show', $customer) }}">
                           
                            <i class="fas fa-eye mr-2"></i>
                            Details

                           </a>
                           <a class="btn btn-outline-secondary btn-sm" href="{{ route('admin.customers.edit', $customer) }}">
                           
                            <i class="fas fa-edit mr-2"></i>
                            Edit

                           </a>

                           <form style="display: inline-block" action="{{ route('admin.customers.destroy', $customer) }}" method="post" enctype="multipart/form-data">
                              @csrf
                              @method('DELETE')

                              <button class="btn btn-outline-danger btn-sm">
                                  <i class="fas fa-trash mr-2"></i>
                                  Delete
                              </button>
                          </form>
                      </td>
                  </tr>
                      
                  @endforeach
              </tbody>
          </table>
          @if(count($customers) == 0)
          <div class="alert alert-warning text-center mb-0">
              No Items Found!
          </div>
          @endif
          <div class="card-fotter">       
                 {{ $customers->links() }}
                </div>
          



      </div>

</div>

@endsection
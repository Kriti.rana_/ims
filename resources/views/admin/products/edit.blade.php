@extends('adminlte::page')

@section('title', 'Edit Product')

@section('content')

<x-alert />

<div class="card">
    <div class="card-header">

        <h3 class="card-title" style="font-size: 1.5rem;line-height:1.8;font-weight:bold">
            Edit Product
        </h3>

        <div class="card-tools">
            <a class="btn btn-outline-secondary" href="{{ route('admin.products.index') }}"> 
                <i class="fas fa-arrow-circle-left mr-2"></i>
                Go Back
            </a>
        </div>

    </div>

      <div class="card-body">
          <form action="{{ route('admin.products.update', $product) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" class="form-control @error('name')
                is-invalid
            @enderror" value="{{ old('name') ?? $product->name }}">

            @error('name')
                    <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>

            <div class="form-group">
                <label for="category_id">Category</label>

                <select name="category_id" id="category_id" class="form-control">

                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}" @if ($product->category_id == $category->id)
                            selected 
                        @endif>{{ $category->name }}</option>
                    @endforeach

                </select>

                @error('category_id')
                    <small class="form-text text-danger">{{ $message }}</small>
                @enderror

            </div>

           <div class="form-group">
                <label for="supplier_id">Supplier</label>

                <select name="supplier_id" id="supplier_id" class="form-control">

                    @foreach ($suppliers as $supplier)
                        <option value="{{ $supplier->id }}" @if ($product->supplier_id == $supplier->id)
                            selected 
                        @endif>{{ $supplier->name }}</option>
                    @endforeach

                </select>

                @error('supplier_id')
                    <small class="form-text text-danger">{{ $message }}</small>
                @enderror

            </div>

            

            <div class="form-group">
                <label for="code">Code</label>
                <input type="text" name="code" id="code" class="form-control @error('code')
                is-invalid
            @enderror" value="{{ old('code') ?? $product->code }}">

            @error('code')
                    <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>

            <div class="form-group">
                <label for="buying_price">Buying Price</label>
                <input type="text" name="buying_price" id="buying_price" class="form-control @error('buying_price')
                is-invalid
            @enderror" value="{{ old('buying_price') ?? $product->buying_price }}">

            @error('buying_price')
                    <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>

            <div class="form-group">
                <label for="selling_price">Selling Price</label>
                <input type="text" name="selling_price" id="selling_price" class="form-control @error('selling_price')
                is-invalid
            @enderror" value="{{ old('selling_price') ?? $product->selling_price }}">

            @error('selling_price')
                    <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>

            <div class="form-group">
                <label for="quantity">Quantity</label>
                <input type="text" name="quantity" id="quantity" class="form-control @error('quantity')
                is-invalid
            @enderror" value="{{ old('quantity') ?? $product->quantity }}">

            @error('quantity')
                    <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>

            <div class="form-group">
                <label for="buying_date">Buying Date</label>
                <input type="date" name="buying_date" id="buying_date" class="form-control @error('buying_date') is-invalid
                    
                @enderror" value="{{ old('buying_date') ?? $product->buying_date }}">

                @error('buying_date')
                    <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>


            <button class="btn btn-outline-secondary">
                <i class="fas fa-save mr-2"></i>
                Save
            </button>
          </form>
      </div>

</div>

@endsection
@extends('adminlte::page')

@section('title', 'Product Details')

@section('content')

<x-alert />

<div class="card">
    <div class="card-header">

        <h3 class="card-title" style="font-size: 1.5rem;line-height:1.8;font-weight:bold">
            Product Details
        </h3>

        <div class="card-tools">
            <a class="btn btn-outline-secondary" href="{{ route('admin.products.index') }}"> 
                <i class="fas fa-arrow-circle-left mr-2"></i>
                Go Back
            </a>
        </div>

    </div>

      <div class="row card-body p-0">
          <div class="col">
          <table class="table table-bordered table-striped">
              <tr>
                  <th style="width: 50%">ID</th>
                  <td>{{ $product->id }}</td>

              </tr>
              <tr>
                <th>Name</th>
                <td>{{ $product->name }}</td>
                
            </tr>
            <tr>
                <th>Stock</th>
                <td>{{ $product->stocks_sum_quantity ?? 0 }}</td>
            </tr>
            <tr>
                <th>Category</th>
                <td>{{ $product->category->name }}</td>
                
            </tr>
            <tr>
                <th>Created</th>
                <td>{{ $product->created_at }}</td>
                
            </tr>
            <tr>
                <th>Updated</th>
                <td>{{ $product->updated_at }}</td>
                
            </tr>
          </table>
          
      </div>

      <div class="col">
          <h1 style="text-align: center">{{ $product->name }}</h1>
             @if ($product->media)
                <img src="/storage/{{ $product->media->path }}" height="500px" />
            @endif

      </div>
     
      
      </div>

</div>

@endsection
@extends('adminlte::page')

@section('title', 'All BatchProducts')

@section('content')

<x-alert />

<div class="card">
    <div class="card-header">

        <h3 class="card-title" style="font-size: 1.5rem;line-height:1.8;font-weight:bold">
            All Batch Products
        </h3>

        <div class="card-tools">

            


            <a class="btn btn-outline-secondary" href="{{ route('admin.batchproducts.create') }}"> <i class="fas fa-plus-circle mr-2"></i>Add New</a>
        </div>

    </div>

      <div class="card-body p-0">
          <table class="table table-bordered table-striped">
              <thead class="bg-secondary">
                  <tr>
                      <th>ID</th>
                      <th>Batch</th>
                      <th>Product</th>
                      <th>Quantity</th>
                      <th>Unit Price</th>
                      <th>Action</th>
                  </tr>
              </thead>

              <tbody>
                  @foreach ($batchproducts as $batchproduct)
                  <tr>
                      <td>{{ $batchproduct->id }}</td>
                      <td>{{ $batchproduct->batch->name }}</td>
                      <td>{{ $batchproduct->product->name }}</td>
                      <td>{{ $batchproduct->quantity }}</td>
                      <td>{{ $batchproduct->unit_price }}</td>
                      
                      <td>
                          <a class="btn btn-outline-secondary btn-sm" href="{{ route('admin.batchproducts.show', $batchproduct) }}">
                           
                            <i class="fas fa-eye mr-2"></i>
                            Details

                           </a>
                           <a class="btn btn-outline-secondary btn-sm" href="{{ route('admin.batchproducts.edit', $batchproduct) }}">
                           
                            <i class="fas fa-edit mr-2"></i>
                            Edit

                           </a>
                           <form style="display: inline-block" action="{{ route('admin.batchproducts.destroy', $batchproduct) }}" method="post">
                            @csrf
                            @method('DELETE')

                            <button class="btn btn-outline-danger btn-sm">
                                <i class="fas fa-trash mr-2"></i>
                                Delete
                            </button>
                        </form>

                          
                      </td>
                  </tr>
                      
                  @endforeach
              </tbody>
          </table>

          @if(count($batchproducts) == 0)
          <div class="alert alert-warning text-center mb-0">
              No Items Found!
          </div>
          @endif

          
        </div>
        <div class="card-footer">
            {{ $batchproducts->links() }}
        </div>
</div>

@endsection
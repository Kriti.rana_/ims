@extends('adminlte::page')

@section('title', 'Batch Product Details')

@section('content')

<x-alert />

<div class="card">
    <div class="card-header">

        <h3 class="card-title" style="font-size: 1.5rem;line-height:1.8;font-weight:bold">
           Batch Product Details
        </h3>

        <div class="card-tools">
            <a class="btn btn-outline-secondary" href="{{ route('admin.batchproducts.index') }}"> 
                <i class="fas fa-arrow-circle-left mr-2"></i>
                Go Back
            </a>
        </div>

    </div>

      <div class="row card-body p-0">
          
          <table class="table table-bordered table-striped">
              <tr>
                  <th style="width: 50%">ID</th>
                  <td>{{ $batchproduct->id }}</td>

              </tr>
            <tr>
                <th>Batch</th>
                <td>{{ $batchproduct->batch->name }}</td>
                
            </tr>
            <tr>
                <th>Product</th>
                <td>{{ $batchproduct->product->name }}</td>
                
            </tr>
           
            <tr>
                <th>Quantity</th>
                <td>{{ $batchproduct->quantity }}</td>
                
            </tr>
            <tr>
                <th>Unit Price</th>
                <td>{{ $batchproduct->unit_price }}</td>
                
            </tr>
            
            <tr>
                <th>Created</th>
                <td>{{ $batchproduct->created_at }}</td>
                
            </tr>
            <tr>
                <th>Updated</th>
                <td>{{ $batchproduct->updated_at }}</td>
                
            </tr>
          </table>
          
        </div>

</div>

@endsection
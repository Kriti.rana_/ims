@extends('adminlte::page')

@section('title', 'Add New BatchProduct')
@section('plugins.Select2', true)

@section('js')
<script>
$(document).ready(function() {
    $('#product_id').select2();
});
</script>
@endsection

@section('content')

<x-alert />

<div class="card">
    <div class="card-header">

        <h3 class="card-title" style="font-size: 1.5rem;line-height:1.8;font-weight:bold">
            Add New BatchProduct
        </h3>

        <div class="card-tools">
            <a class="btn btn-outline-secondary" href="{{ route('admin.batchproducts.index') }}"> <i class="fas fa-arrow-circle-left mr-2"></i>Go Back</a>
        </div>

    </div>

      <div class="card-body">
          <form action="{{ route('admin.batchproducts.store') }}" method="post">
            @csrf

            <div class="form-group">
                <label for="batch_id">Batch</label>
                <select name="batch_id" id="batch_id" class="form-control @error('batch_id')
                is-invalid
            @enderror">

            @foreach ($batches as $batch)
            <option value="{{ $batch->id }}">{{ $batch->name }}</option>
            @endforeach

                </select>

            @error('batch_id')
                    <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>

            <div class="form-group">
                <label for="product_id">Product</label>
                <select name="product_id" id="product_id" class="form-control @error('product_id')
                is-invalid
            @enderror">

            @foreach ($products as $product)
            <option value="{{ $product->id }}">{{ $product->name }}</option>
            @endforeach

                </select>


                @error('product_id')
                    <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div> 

            <div class="form-group">
                <label for="quantity">Quantity</label>
                <input type="number" name="quantity" id="quantity" placeholder="0" class="form-control @error('quantity')
                is-invalid
            @enderror" value="{{ old('quantity') }}">

                @error('quantity')
                    <small class="form-text text-danger">{{ $message }}</small>
                @enderror

            </div>

            <div class="form-group">
                <label for="unit_price">Unit Price</label>
                <input type="number" name="unit_price" id="unit_price" placeholder="0" class="form-control @error('unit_price')
                is-invalid
            @enderror" value="{{ old('unit_price') }}">

            @error('unit_price')
                    <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>

            <button class="btn btn-outline-secondary">
                <i class="fas fa-save mr-2"></i>
                Save
            </button>
          </form>
      </div>

</div>

@endsection
@extends('adminlte::page')
@section('title','Add New Order')
@section('plugins.Select2', true)

@section('js')
<script>
$(document).ready(function() {
    $('#product_id').select2();
    $('#customer_id').select2();
});
</script>
@endsection

@section('content')

<x-alert />

<div class="card">
    <div class="card-header">
        <h3 class="card-title" style="font-size:1.3rem;line-height:1.8;
        font-weight:bold">
            Add New Order
        </h3>
        <div class="card-tools">
            <a class="btn btn-outline-secondary" href="{{ route('admin.orders.index') }}">
            <i class="fas fa-arrow-circle-left mr-2"></i>
                Go Back
            </a>
        </div>
    </div>

    <div class="card-body">
        <form action="{{ route('admin.orders.store') }}" method="post">
        @csrf

        <div class="form-group">
            <label for="user_id">Customer</label>
            <select name="customer_id" id="customer_id" class="form-control">
                @foreach ($customers as $customer)
                    <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="product_id">Choose a products</label>
            <select name="product_id" id="product_id" class="form-control">
                @foreach ($products as $product)
                    <option value="{{ $product->id }}">{{ $product->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="quantity">Quantity</label>
            <input type="number" min="0" value="{{ old('quantity') ?? 1 }}" name="quantity" id="quantity" class="form-control">
        </div>

        <button class="btn btn-outline-secondary">
            <i class="fas fa-save mr-2"></i>
            Save
        </button>
        </form>
    </div>
</div>

@endsection
@extends('adminlte::page')
@section('title','All Orders')

@section('content')

<x-alert />

<div class="card">
    <div class="card-header">
        <h3 class="card-title" style="font-size:1.3rem;line-height:1.8;
        font-weight:bold">
            Orders and Sales
        </h3>
        <div class="card-tools">
            <a class="btn btn-outline-secondary" href="{{ route('admin.orders.create') }}">
            <i class="fas fa-plus-circle mr-2"></i>
                Add New
            </a>
        </div>
    </div>

    <div class="card-body p-0">
        <table class="table table-bordered">
           <thead class="bg-secondary">
        <tr>
            <th>ID</th>
            <th>Customer</th>
            <th>Status</th>
            <th>Date</th>
            <th>Action</th>
        </tr>
        </thead>

        <tbody>
            @foreach($orders as $order)
            <tr>
                <td>{{ $order->id }}</td>
                <td>{{ $order->customer_id ? $order->customer->name : 'Unknown Customer' }}</td>
                <td>{{ $order->status }}</td>
                <td>{{ $order->created_at }}</td>
                <td>
                    <a class="btn btn-primary btn-sm" href="{{route('admin.orders.show', $order)}}">
                    <i class="fas fa-eye mr-2"></i>
                        Details
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
        </table>
        @if(count($orders) == 0)
          <div class="alert alert-warning text-center mb-0">
              No Items Found!
          </div>
          @endif
          <div class="card-fotter">
                 {{ $orders->links() }}
                </div>
    </div>
</div>

@endsection
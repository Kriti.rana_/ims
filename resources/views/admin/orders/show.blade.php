@extends('adminlte::page')
@section('title','Order Details')

@section('plugins.Select2', true)

@section('js')
<script>
$(document).ready(function() {
    $('#product_id').select2();
});
</script>
@endsection

@section('content')

<x-alert />

<div class="card">
    <div class="card-header">
        <h3 class="card-title" style="font-size:1.3rem;line-height:1.8;font-weight:bold">
            Order Details
        </h3>
        <div class="card-tools">
            {{-- <form class="block-margin mr-3" style="display: inline-block!important;" action="{{ route('admin.orders.status', $order) }}" method="post">
                @csrf
                <div class="input-group">
                    <x-input
                    type="select"
                    field="status"
                    text=""
                    :options="\App\Models\Order::STATUS"
                    key=""
                    :current="$order->status"
                    :error="false"
                />
                <div class="input-group-append">
                    <button type="submit" class="btn btn-info">Update</button>
                </div>
                </div>

            </form> --}}

            <a class="btn btn-outline-secondary" href="{{ route('admin.orders.index') }}">
            <i class="fas fa-arrow-circle-left mr-2"></i>
                Go Back
            </a>
        </div>
    </div>

    @if($order->can_update)
    <div class="card-body">
        <form action="{{ route('admin.orders.products.store', $order) }}" method="post">
            @csrf

            <div class="row">
                <div class="col-md-7">
                    <x-input
                        type="select"
                        field="product_id"
                        text="Product"
                        :options="$products"
                    />
                </div>
                <div class="col-md-3">
                    <x-input
                        type="number"
                        field="quantity"
                        text="Quantity"
                        :current="old('quantity') ?? 1"
                    />
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="">&nbsp;</label><br>
                        <button type="submit" class="btn btn-primary">
                            Add products
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    @else
    <div class="card-footer">
        <div class="alert alert-warning mb-0 text-center">
            This order has been marked as {{ $order->status }}. No further modification can be made!
        </div>
    </div>
    @endif


    <div class="card-body p-0">
        <table class="table table-bordered">
            <thead class="bg-primary">
                <tr>
                    <th>S.N</th>
                    <th>Product</th>
                    <th>Quantity</th>
                    <th>Unit Price</th>
                    <th>Total</th>
                    @if($order->can_update)
                    <th>Action</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @forelse($product_orders as $po)
                <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $po->product->name }}</td>
                    <td>{{ $po->quantity }}</td>
                    <td>Rs. {{ $po->unit_price }}</td>
                    <td>Rs. {{ $po->quantity * $po->unit_price }}</td>
                    @if($order->can_update)
                    <td>
                        <a class="btn btn-sm btn-primary" href="{{ route('admin.orders.products.quantity', [$order, $po]) }}">
                            <i class="fas fa-fw fa-edit mr-2"></i>
                            Qty
                        </a>
                        <a class="btn btn-sm btn-danger" href="{{ route('admin.orders.products.delete', [$order, $po]) }}">
                            <i class="fas fa-fw fa-trash"></i>
                        </a>
                    </td>
                    @endif
                </tr>
                @empty
                <tr>
                    <td colspan="6" style="text-align: center">
                        There are no products added to this order. Please add some from above and it will appear here!
                    </td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    @if($order->can_update)
    <div class="card-footer">
        <form action="{{ route('admin.orders.status', $order) }}" method="post">
            @csrf
            <input type="hidden" name="status" value="Complete">
            <button type="submit" class="btn btn-primary btn-lg">
                Complete Order
            </button>
        </form>
    </div>
    @endif
</div>

<style>
    .block-margin .form-group {
        margin-bottom: 0!important;
    }
</style>
@endsection
@extends('adminlte::page')

@section('title', 'Batch Details')

@section('content')

<x-alert />

<div class="card">
    <div class="card-header">

        <h3 class="card-title" style="font-size: 1.5rem;line-height:1.8;font-weight:bold">
            Batch Details
        </h3>

        <div class="card-tools">
            <a class="btn btn-outline-secondary" href="{{ route('admin.batches.index') }}"> 
                <i class="fas fa-arrow-circle-left mr-2"></i>
                Go Back
            </a>
        </div>

    </div>

      <div class="row card-body p-0">
          <div class="col">
          <table class="table table-bordered table-striped">
              <tr>
                  <th style="width: 50%">ID</th>
                  <td>{{ $batch->id }}</td>

              </tr>
              <tr>
                <th>Name</th>
                <td>{{ $batch->name }}</td>
                
            </tr>
            <tr>
                <th>Supplier</th>
                <td>{{ $batch->supplier->name }}</td>
                
            </tr>
            
            
            <tr>
                <th>Created</th>
                <td>{{ $batch->created_at }}</td>
                
            </tr>
            <tr>
                <th>Updated</th>
                <td>{{ $batch->updated_at }}</td>
                
            </tr>
          </table>
          
      </div>
     
      </div>

</div>

@endsection
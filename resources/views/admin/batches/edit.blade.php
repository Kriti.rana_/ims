@extends('adminlte::page')

@section('title', 'Edit Batch')

@section('content')

<x-alert />

<div class="card">
    <div class="card-header">

        <h3 class="card-title" style="font-size: 1.5rem;line-height:1.8;font-weight:bold">
            Edit Batch
        </h3>

        <div class="card-tools">
            <a class="btn btn-outline-secondary" href="{{ route('admin.batches.index') }}"> 
                <i class="fas fa-arrow-circle-left mr-2"></i>
                Go Back
            </a>
        </div>

    </div>

      <div class="card-body">
          <form action="{{ route('admin.batches.update', $batch) }}" method="post">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" class="form-control @error('name')
                is-invalid
            @enderror" value="{{ old('name') ?? $batch->name }}">

            @error('name')
                    <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>

            <div class="form-group">
                <label for="supplier_id">Batch</label>

                <select name="supplier_id" id="supplier_id" class="form-control">

                    @foreach ($suppliers as $supplier)
                        <option value="{{ $supplier->id }}" @if ($supplier->supplier_id == $supplier->id)
                            selected 
                        @endif>{{ $supplier->name }}</option>
                    @endforeach

                </select>

                @error('supplier_id')
                    <small class="form-text text-danger">{{ $message }}</small>
                @enderror

            </div>


            <button class="btn btn-outline-secondary">
                <i class="fas fa-save mr-2"></i>
                Save
            </button>
          </form>
      </div>

</div>

@endsection
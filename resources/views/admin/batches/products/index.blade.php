@extends('adminlte::page')

@section('title', 'Batch Products')
@section('plugins.Select2', true)

@section('js')
<script>
$(document).ready(function() {
    $('#product_id').select2();
});
</script>
@endsection


@section('content')

<x-alert />

<div class="card">
    <div class="card-header">

        <h3 class="card-title" style="font-size: 1.5rem;line-height:1.8;font-weight:bold">
           Batch Products 
        </h3>

        <div class="card-tools">
            <a class="btn btn-outline-secondary" href="{{ route('admin.batches.index') }}">
                 <i class="fas fa-arrow-circle-left mr-2"></i>Go Back</a>
        </div>

    </div>

    

    <div class="card-body">
        <form action="{{ route('admin.batches.products.store', $batch) }}" method="post">
          @csrf

          <div class="form-group">
              <label for="product_id">Product</label>
              <select name="product_id" id="product_id" class="form-control">
                  <option value="">Choose one...</option>
                  @foreach($products as $product)
                    <option value="{{ $product->id }}">{{ $product->name }}</option>
                  @endforeach
              </select>
          </div>

        
          <div class="form-group">
              <label for="quantity">Quantity</label>
              <input type="number" name="quantity" id="quantity" value="1" class="form-control"> 
          </div>

          <div class="form-group">
              <label for="unit_price">Unit Price</label>
              <input type="number" name="unit_price" id="unit_price" class="form-control">
          </div>


          <button class="btn btn-outline-secondary">
              <i class="fas fa-save mr-2"></i>
              Add Product
          </button>
        </form>
    </div>
    

    

      <div class="card-body p-0">
          <table class="table table-bordered table-striped">
              <thead class="bg-secondary">
                  <tr>
                      <th>Product</th>
                      <th>Quantity</th>
                      <th>Unit Price</th>
                      <th>Total</th>
                      <th>Action</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($batch_products as $bp)
                <tr>
                    <td>{{ $bp->product->name }}</td>
                    <td>{{ $bp->quantity }}</td>
                    <td>{{ $bp->unit_price }}</td>
                    <td>{{ $bp->quantity * $bp->unit_price }}</td>
                    <td>
                        <form action="{{ route('admin.batches.products.destory', [$batch, $bp->id]) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-sm btn-danger">
                                <i class="fas fa-fw fa-trash mr-2"></i>
                                <span>Delete</span>
                            </button>

                        </form>
                    </td>
                </tr>
                @endforeach
              </tbody>

              
          </table>

      </div>

    </div>

@endsection
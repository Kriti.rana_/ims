@extends('adminlte::page')

@section('title', 'All Batches')

@section('content')

<x-alert />


<div class="card">
    <div class="card-header">

        <h3 class="card-title" style="font-size: 1.5rem;line-height:1.8;font-weight:bold">
            All Batches
        </h3>

        <div class="card-tools">

            


            <a class="btn btn-outline-secondary" href="{{ route('admin.batches.create') }}"> <i class="fas fa-plus-circle mr-2"></i>Add New</a>
        </div>

    </div>

      <div class="card-body p-0">
          <table class="table table-bordered table-striped">
              <thead class="bg-secondary">
                  <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Supplier</th>
                      <th>Action</th>
                  </tr>
              </thead>

              <tbody>
                  @foreach ($batches as $batch)
                  <tr>
                      <td>{{ $batch->id }}</td>
                      <td>{{ $batch->name }}</td>
                      <td>{{ $batch->supplier->name }}</td>
                      
                      <td>
                          <a class="btn btn-outline-secondary btn-sm" href="{{ route('admin.batches.show', $batch) }}">
                           
                            <i class="fas fa-eye mr-2"></i>
                            Details

                           </a>
                           <a class="btn btn-outline-secondary btn-sm" href="{{ route('admin.batches.products.index', $batch) }}">
                           
                            <i class="fas fa-truck-loading mr-2"></i>
                            Products

                           </a>
                           <a class="btn btn-outline-secondary btn-sm" href="{{ route('admin.batches.edit', $batch) }}">
                           
                            <i class="fas fa-edit mr-2"></i>
                            Edit

                           </a>
                           <form style="display: inline-block" action="{{ route('admin.batches.destroy', $batch) }}" method="post">
                            @csrf
                            @method('DELETE')

                            <button class="btn btn-outline-danger btn-sm">
                                <i class="fas fa-trash mr-2"></i>
                                Delete
                            </button>
                        </form>
                          
                      </td>
                  </tr>
                      
                  @endforeach
              </tbody>
          </table>

          @if(count($batches) == 0)
          <div class="alert alert-warning text-center mb-0">
              No Items Found!
          </div>
          @endif
      </div>

      <div class="card-footer">
        {{ $batches->links() }}
      </div>

</div>

@endsection
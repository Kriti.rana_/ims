@extends('adminlte::page')

@section('title', 'Add New Batch')
@section('plugins.Select2', true)

@section('js')
<script>
$(document).ready(function() {
    $('#supplier_id').select2();

});
</script>
@endsection

@section('content')

<x-alert />

<div class="card">
    <div class="card-header">

        <h3 class="card-title" style="font-size: 1.5rem;line-height:1.8;font-weight:bold">
            Add New Batch
        </h3>

        <div class="card-tools">
            <a class="btn btn-outline-secondary" href="{{ route('admin.batches.index') }}"> <i class="fas fa-arrow-circle-left mr-2"></i>Go Back</a>
        </div>

    </div>

      <div class="card-body">
          <form action="{{ route('admin.batches.store') }}" method="post" enctype="multipart/form-data">
            @csrf

            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" class="form-control @error('name')
                    is-invalid
                @enderror" value="{{ old('name') }}">

                @error('name')
                    <small class="form-text text-danger">{{ $message }}</small>
                @enderror

            </div>

            <div class="form-group">
                <label for="supplier_id">Supplier</label>
                <select name="supplier_id" id="supplier_id" class="form-control @error('supplier_id')
                is-invalid
            @enderror">

            @foreach ($suppliers as $supplier)
            <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
            @endforeach

                </select>


                @error('supplier_id')
                    <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>


            <button class="btn btn-outline-secondary">
                <i class="fas fa-save mr-2"></i>
                Create New Batch
            </button>
          </form>
      </div>

</div>

@endsection
@extends('adminlte::page')

@section('title', 'Category Details')

@section('content')

<x-alert />

<div class="card">
    <div class="card-header">

        <h3 class="card-title" style="font-size: 1.5rem;line-height:1.8;font-weight:bold">
            Category Details
        </h3>

        <div class="card-tools">
            <a class="btn btn-outline-secondary" href="{{ route('admin.categories.index') }}"> 
                <i class="fas fa-arrow-circle-left mr-2"></i>
                Go Back
            </a>
        </div>

    </div>

      <div class="card-body p-0">
          <table class="table table-bordered table-striped">
              <tr>
                  <th style="width: 20%">ID</th>
                  <td>{{ $category->id }}</td>

              </tr>
              <tr>
                <th>Name</th>
                <td>{{ $category->name }}</td>
                
            </tr>
            <tr>
                <th>Created</th>
                <td>{{ $category->created_at }}</td>
                
            </tr>
            <tr>
                <th>Updated</th>
                <td>{{ $category->updated_at }}</td>
                
            </tr>
          </table>
          
      </div>

</div>

@endsection
@extends('adminlte::page')

@section('content')
@section('title','All Stocks')
@section('content')
<x-alert />

<div class="card">
    <div class="card-header">
        <h3 class="card-title" style="font-size:1.3rem;line-height:1.8;
        font-weight:bold">
            All Stocks
        </h3>
         <div class="card-tools">
            <a class="btn btn-outline-secondary" href="{{ route('admin.stocks.create') }}">
            <i class="fas fa-plus-circle mr-2"></i>
                Add New
            </a>
        </div>
    </div>

    <div class="card-body p-0">
        <table class="table table-bordered">
           <thead class="bg-secondary">
        <tr>
            <th>ID</th>
            <th>Product</th>
            <th>Type</th>
            <th>Quantity</th>
        </tr>
        </thead>
        <tbody>
            @foreach($stocks as $stock)
            <tr>
                <td>{{ $loop->index+1 }}</td>
                <td>{{ $stock->product->name }}</td>
                <td>{{ $stock->quantity <= 0 ? "Sales" : "Import" }}</td>
                <td>{{ abs($stock->quantity) }}</td>
                
            </tr>
            @endforeach
        </tbody>
        </table>
    </div>
</div>
@endsection
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Point of Sale</title>
    <link rel="stylesheet" href="{{ asset('/vendor/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/fontawesome-free/css/all.min.css') }}">
    <style>
        .select2-container .select2-selection--single {
            height: 33.33px;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 33.33px;
        }
    </style>
    @yield('css')
</head>
<body class="bg-light">

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container">
      <a class="navbar-brand" href="{{ route('pos.index') }}">POS</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarColor01">
        <ul class="navbar-nav me-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('pos.index') }}">Home</a>
            </li>
            @if(auth()->user()->role == "Admin")
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.index') }}">
                    Admin Dashboard
                </a>
            </li>
            @endif
        </ul>
        <form class="d-flex" method="POST" action="{{ route('logout') }}">
            @csrf
          <button class="btn btn-danger my-2 my-sm-0" type="submit">Log Out</button>
        </form>
      </div>
    </div>
  </nav>

  <div class="container py-4">
    @yield('content')
  </div>

    <!-- Script -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
    @yield('js')
</bodyv>
</html>
@extends('pos.layout')
@section('js')
<script>
    $(document).ready(function() {
        $('#customer_id').select2();
    });
</script>
@endsection

@section('content')

<h3 class="text-center mt-3 mb-4 fw-bold">
    Point of Sale
</h3>
<div class="row">
    <div class="col-12 col-md-6">
        <div class="card mb-3">
            <div class="card-header">Existing Customer</div>
            <div class="card-body">
                <form action="{{ route('pos.customer.select') }}" method="post">
                    @csrf

                    <x-input
                        type="select"
                        field="customer_id"
                        text="Customer"
                        :options="$customers"
                        :required="true"
                    />

                    <button class="btn btn-primary">
                        Create Order from Customer
                    </button>

                </form>
            </div>
            <div class="card-footer">
                <form action="{{ route('pos.customer.none') }}" method="post">
                    @csrf
                    <button class="btn btn-info w-100">
                        Create Order without Customer
                    </button>

                </form>
            </div>
        </div>

        <div class="card">
            <div class="card-header">Recent Order</div>
            <div class="card-body p-0">
                <table class="table table-bordered m-0">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Customer</th>
                            <th>Total</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($orders as $order)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>{{ $order->customer_id ? $order->customer->name : 'Unknown Customer' }}</td>
                            <td>Rs. {{ $order->total }}</td>
                            <td>
                                <a href="{{ route('pos.order', $order) }}" class="btn btn-secondary">
                                    View
                                </a>
                            </td>
                        </tr>

                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6">
        <div class="card">
            <div class="card-header">New Customer</div>
            <div class="card-body">
                <form action="{{ route('pos.customer.create') }}" method="post">
                    @csrf

                    <x-input
                        type="text"
                        field="name"
                        text="Customer Name"
                        :required="true"
                    />

                    <x-input
                        type="text"
                        field="email"
                        text="Email Address"
                        :required="true"
                    />

                    <x-input
                        type="number"
                        field="phone"
                        text="Phone Number"
                        :required="true"
                    />

                    <x-input
                        type="text"
                        field="address"
                        text="Address"
                        :required="true"
                    />

                    <button class="btn btn-primary">
                        Add Customer and Create Order
                    </button>

                </form>
            </div>
        </div>
    </div>
</div>

@endsection
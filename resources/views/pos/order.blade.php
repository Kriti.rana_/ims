@extends('pos.layout')
@section('js')
<script>
    $(document).ready(function() {
        $('#product_id').select2();
    });

    function increaseQty() {
        let qty = Number(document.getElementById('quantity').value);
        document.getElementById('quantity').value = qty + 1;
    }

    function decreaseQty() {
        let qty = Number(document.getElementById('quantity').value);
        if (qty <= 1) return;
        document.getElementById('quantity').value = qty - 1;
    }

    function updateQuantity(id, quantity) {
        document.getElementById('op_id').value = id;
        document.getElementById('op_quantity').value = quantity;

        $('#updateModal').modal('show');
    }

    function confirmDelete(order, op) {
        if(confirm('Are you sure?')) {
            let url = "/pos/order/" + order+ "/" + op + "/delete";
            location.href = url;
        }
    }
</script>
@endsection
@section('content')

<div class="card">
    @if($order->can_update)
    <div class="card-body">
        <form action="{{ route('pos.products.store', $order) }}" method="post">
            @csrf

            <div class="row">
                <div class="col-md-7">
                    <x-input
                        type="select"
                        field="product_id"
                        text="Product"
                        :options="$products"
                    />
                </div>
                <div class="col-md-3">
                    <label for="quantity" class="form-label">Quantity</label>
                        <div class="input-group">
                            <button type="button" class="input-group-text" onclick="decreaseQty()">
                                <i class="fa fa-minus fa-fw"></i>
                            </button>
                            <input
                                type="text"
                                name="quantity" id="quantity"
                                value="{{ old('quantity') ?? 1 }}"
                                style="text-align: center;font-weight: bold"
                                class="form-control @error('quantity') is-invalid @enderror"
                            >
                            <button type="button" class="input-group-text" onclick="increaseQty()">
                                <i class="fa fa-plus fa-fw"></i>
                            </button>
                        </div>
                        @error('quantity')
                        <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="form-label" for="">&nbsp;</label><br>
                        <button type="submit" class="btn btn-primary">
                            Add products
                        </button>
                    </div>
                </div>
            </div>
        </form>
        @else
        <div class="card-footer">
            <div class="alert alert-warning mb-0 text-center">
                This order has been marked as {{ $order->status }}. No further modification can be made!
            </div>
        </div>
        @endif
    </div>
    <div class="card-body p-0">
        <table class="table table-bordered m-0">
            <thead class="bg-primary text-white">
                <tr>
                    <th>S.N</th>
                    <th>Product</th>
                    <th>Quantity</th>
                    <th>Unit Price</th>
                    <th>Total</th>
                    @if($order->can_update)
                    <th>Action</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @php $total = 0; @endphp
                @foreach($product_orders as $po)
                <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $po->product->name }}</td>
                    <td>{{ $po->quantity }}</td>
                    <td>Rs. {{ $po->unit_price }}</td>
                    <td>Rs. {{ $po->quantity * $po->unit_price }}</td>
                    @php $total += $po->quantity * $po->unit_price; @endphp
                    @if($order->can_update)
                    <td>
                        <button
                            type="button"
                            class="btn btn-sm btn-primary mr-2"
                            onclick="updateQuantity({{ $po->id }}, {{ $po->quantity }})"
                            data-toggle="tooltip" data-placement="top"
                            title="Update Quantity"
                        >
                            <i class="fas fa-fw fa-edit"></i>
                        </button>

                        <button
                        class="btn btn-sm btn-danger"
                        onclick="confirmDelete({{ $order->id }}, {{ $po->id }})">
                            <i class="fas fa-fw fa-trash"></i>
                        </button>
                    </td>
                    @endif
                </tr>
                @endforeach
                <tr>
                    <th colspan="4" style="text-align: right">Total</th>
                    <th>Rs. {{ $total }}</th>
                </tr>

                @if(count($product_orders) == 0)
                <tr>
                    <td colspan="6" style="text-align: center">
                        <div class="alert alert-warning text-center m-0">
                            There are no products added to this order. Please add some from above and it will appear here!
                        </div>
                    </td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
    @if($order->can_update)
    <div class="card-footer">
        <form action="{{ route('pos.status', $order) }}" method="post">
            @csrf
            <input type="hidden" name="status" value="Complete">
            <button type="submit" class="btn btn-primary btn-lg">
                Complete Order
            </button>
        </form>
    </div>
    @endif
</div>

<!-- Update Quantity Modal -->
<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="POST" action="{{ route('pos.quantity', $order) }}">
                @csrf
                <input id="op_id" name="order_product_id" type="hidden" value="0">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update Quantity</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="op_quantity" class="form-label">Quantity</label>
                        <input
                            type="number"
                            class="form-control"
                            name="quantity" id="op_quantity"
                        >
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update Quantity</button>
                </div>
            </form>
        </div>
    </div>
</div>



@endsection
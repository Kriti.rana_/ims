<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
</head>

<body style="background: url({{ asset('images/inven.jpeg') }})">
    <div class="box d-flex align-items-center justify-content-center">
        <div class="card" style="width: 400px">
            <div class="card-header">
                <h4 class="text-center">Login</h4>
            </div>
            <div class="card-body">

                <form action="{{ route('login') }}" method="post">
                    @csrf

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            {{ $errors->first() }}
                        </div>
                    @endif

                    <x-input type="email" field="email" text="Email Address" />

                    <x-input type="password" field="password" text="Password" />

                    <div class="mb-3 d-flex">
                        <input type="checkbox" class="form-check-input me-2" name="remember" id="remember">
                        <label for="remember" class="form-label">Remember Me</label>
                    </div>

                    <button style="width: 100%;text-align:center" class="btn btn-primary">
                        Login
                    </button>

                </form>
                <p class="mt-3 mb-0">
                    <a href="{{ route('password.request') }}">Forgot Password?</a>
                </p>
                <p class="mt-3 mb-0">
                    <a href="{{ route('register') }}">Create New Account</a>
                </p>


            </div>
        </div>
    </div>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
</body>

</html>

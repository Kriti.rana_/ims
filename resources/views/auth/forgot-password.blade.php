@extends('auth.layout')
@section('title', 'Reset Password')
@section('content')
    <div class="login-box">
        <div class="login-logo">
            <a href="/login" style="flex-direction: column" class="d-flex align-items-center justify-content-center">
                <img src="{{ asset('images/logo.png') }}" style="height: 40px" class="mb-2" />
                <h3>{{ config('app.name') }}</h3>
            </a>
        </div>
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Forgot Password?</p>

                <x-alert />

                <form action="{{ route('password.request') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <div class="input-group mb-3">
                            <input type="email" name="email" autofocus required
                                class="form-control @error('email') is-invalid @enderror" placeholder="Email Address"
                                value="{{ old('email') }}">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                </div>
                            </div>
                        </div>
                        @error('email')
                            <p class="form-text text-danger">{{ $message }}</p>
                        @enderror
                    </div>

                    <div class="mt-4">
                        <button type="submit" class="btn btn-primary btn-block">
                            Send Password Reset Link
                        </button>
                    </div>
                </form>

                <p class="mt-3 mb-0">
                    <a href="{{ route('login') }}">Log In</a>
                </p>
            </div>
        </div>
    </div>
@endsection

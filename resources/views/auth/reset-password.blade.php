@extends('auth.layout')
@section('title', 'Reset Password')
@section('content')
    <div class="login-box">
        <div class="login-logo">
            <a href="/login" style="flex-direction: column" class="d-flex align-items-center justify-content-center">
                <img src="{{ asset('images/logo.png') }}" style="height: 40px" class="mb-2" />
                <h3>{{ config('app.name') }}</h3>
            </a>
        </div>
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Change New Password</p>

                <form action="{{ url('/reset-password') }}" method="post">
                    @csrf
                    <input type="hidden" name="token" value="{{ request()->route('token') }}">
                    <div class="form-group">
                        <div class="input-group mb-3">
                            <input type="email" name="email" autofocus required
                                class="form-control @error('email') is-invalid @enderror" placeholder="Your Email Address"
                                value="{{ old('email') }}">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                </div>
                            </div>
                        </div>
                        @error('email')
                            <p class="form-text text-danger">{{ $message }}</p>
                        @enderror
                    </div>

                    <div class="form-group">
                        <div class="input-group mb-3">
                            <input type="password" name="password" required
                                class="form-control @error('password') is-invalid @enderror" placeholder="Password">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        @error('password')
                            <p class="form-text text-danger">{{ $message }}</p>
                        @enderror
                    </div>


                    <div class="form-group">
                        <div class="input-group mb-3">
                            <input type="password" name="password_confirmation" required
                                class="form-control @error('password_confirmation') is-invalid @enderror"
                                placeholder="Confirm New Password">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        @error('password_confirmation')
                            <p class="form-text text-danger">{{ $message }}</p>
                        @enderror
                    </div>


                    <div class="mt-4">
                        <button type="submit" class="btn btn-primary btn-block">
                            Change Password
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection

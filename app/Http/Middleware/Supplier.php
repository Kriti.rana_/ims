<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Supplier
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if(auth()->user()->role != "Supplier") {
            abort(403);
        }

        if(!auth()->user()->supplier){
            throw new \Exception("Supplier Not Found!");
        }
        
        return $next($request);
    }
}

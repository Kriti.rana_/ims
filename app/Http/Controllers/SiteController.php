<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function index()
    {
        return redirect()->route('home');
    }

    public function home()
    {
        if (auth()->user()->role == "Admin") {
            return redirect('/admin');
        }

        if (auth()->user()->role == "Supplier") {
            return redirect('/supplier');
        }

        return redirect('/404');
    }

    public function notFound()
    {
        return view('404');
    }
}

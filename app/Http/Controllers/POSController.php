<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Stock;
use App\Models\Product;
use App\Models\Customer;
use App\Models\ProductOrder;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class POSController extends Controller
{
    public function index()
    {
        $customers = Customer::all();
        $orders = Order::orderBy('id', 'DESC')->limit(2)->get();

        return view('pos.index', compact('customers', 'orders'));
    }

    public function selectCustomer(Request $request)
    {
        $request->validate([
            'customer_id' => ['required', 'exists:customers,id'],
        ]);

        $customer = Customer::find($request->customer_id);
        $order = $this->createOrder($customer);

        return redirect()->route('pos.order', $order);
    }

    public function emptyCustomer()
    {
        $order = $this->createOrder();

        return redirect()->route('pos.order', $order);
    }

    public function createCustomer(Request $request)
    {
        $data = $request->validate([
            'name' => ['required'],
            'email' => ['required'],
            'address' => ['required'],
            'phone' => ['required'],
        ]);

        $customer = Customer::create($data);

        $order = $this->createOrder($customer);

        return redirect()->route('pos.order', $order);
    }

    public function createOrder(?Customer $customer = null)
    {
        if (!empty($customer)) {
            return Order::create([
                'customer_id' => $customer->id,
            ]);
        }

        return Order::create();
    }

    public function order(Order $order)
    {
        $customer = $order->customer;

        $product_orders = ProductOrder::where('order_id', $order->id)
            ->with(['product'])
            ->get();
        $products = Product::all();

        return view('pos.order', compact('order', 'customer', 'products', 'product_orders'));
    }

    private function checkStock(Product $product, $quantity, $current = 0)
    {
        $product->loadSum('stocks', 'quantity');

        if ($quantity > ($product->stocks_sum_quantity + $current)) {
            return false;
        }

        return true;
    }

    public function addProduct(Request $request, Order $order)
    {
        if (!$order->can_update) {
            return redirect()->route('pos.order', $order)
                ->with('error', 'This order has been marked as ' . $order->status . '! No further modification could be made!');
        }

        $request->validate([
            'product_id' => ['required', 'exists:products,id'],
            'quantity' => ['required', 'integer', 'min:1'],
        ]);

        $product = Product::find($request->product_id);

        // Check if book is already added to order or not
        $check = ProductOrder::where('order_id', $order->id)
            ->where('product_id', $product->id)
            ->first();

        // Check Stock
        if (!$this->checkStock($product, $request->quantity)) {
            $product->loadSum('stocks', 'quantity');
            $stock = $product->stocks_sum_quantity;

            return redirect()->route('pos.order', $order)
                ->with('error', 'This product do not have ' . $request->quantity . ' stock! There are currently: ' . $stock . ' left!');
        }

        if ($check) {

            // If found, add quantity instead of adding another item to order
            $check->update(['quantity' => $check->quantity + $request->quantity]);
            $this->changeStock($product->id, $request->quantity * -1);


            return redirect()->route('pos.order', $order)
                ->with('success', $request->quantity . ' quantity have been added to the order of product: ' . $product->name . '!');
        }

        // If not, proceed as planned to add item to order
        ProductOrder::create([
            'order_id' => $order->id,
            'product_id' => $product->id,
            'quantity' => $request->quantity,
            'unit_price' => $product->selling_price,
        ]);

        $this->changeStock($product->id, $request->quantity * -1);


        return redirect()->route('pos.order', $order)
            ->with('success', 'Products has been added to the order!');
    }

    public function updateQuantity(Request $request, Order $order)
    {
        if (!$order->can_update) {
            return redirect()->route('pos.order', $order)
                ->with('error', 'This order has been marked as ' . $order->status . '! No further modification could be made!');
        }

        $request->validate([
            'order_product_id' => ['required', 'exists:product_order,id'],
            'quantity' => ['required', 'integer', 'min:1'],
        ]);

        $product_order = ProductOrder::find($request->order_product_id);
        $product = $product_order->product;

        // Check Stock
        if (!$this->checkStock($product, $request->quantity, $product_order->quantity)) {
            $product->loadSum('stocks', 'quantity');
            $stock = $product->stocks_sum_quantity + $product_order->quantity;

            return redirect()->route('pos.order', $order)
                ->with('error', 'This product do not have ' . $request->quantity . ' stock! There are currently: ' . $stock . ' left!');
        }

        // Delete Previous Stock (By Adding Them Back)
        Stock::create([
            'product_id' => $product->id,
            'quantity' => $product_order->quantity,
        ]);

        // Decrease stock from new quantity
        Stock::create([
            'product_id' => $product->id,
            'quantity' => $request->quantity * -1,
        ]);

        $product_order->update(['quantity' => $request->quantity]);

        return redirect()->route('pos.order', $order)
            ->with('success', 'Product quantity has been updated!');
    }

    public function deleteProduct(Request $request, Order $order, ProductOrder $product_order)
    {
        if (!$order->can_update) {
            return redirect()->route('pos.order', $order)
                ->with('error', 'This order has been marked as ' . $order->status . '! No further modification could be made!');
        }

        $this->changeStock($product_order->product_id, $product_order->quantity);

        $product_order->delete();

        return redirect()->route('pos.order', $order)
            ->with('success', 'Product has been removed from the order!');
    }

    public function changeStatus(Request $request, Order $order)
    {
        $request->validate([
            'status' => ['required', Rule::in(Order::STATUS)],
        ]);

        $order->update(['status' => $request->status]);

        return redirect()->route('pos.order', $order)
            ->with('success', 'Order status has been updated to: ' . $request->status);
    }

    private function changeStock($product_id, $quantity)
    {
        Stock::create([
            'product_id' => $product_id,
            'quantity' => $quantity,
        ]);
    }
}

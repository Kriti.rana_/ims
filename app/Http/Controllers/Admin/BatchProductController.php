<?php

namespace App\Http\Controllers\Admin;

use App\Models\Batch;
use App\Models\BatchProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;

class BatchProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $batchproducts = BatchProduct::simplePaginate(5);

        return view('admin.batchproducts.index', compact('batchproducts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $batches = Batch::all();
        $products = Product::all();

        return view('admin.batchproducts.create', compact('batches', 'products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'batch_id' => ['required'],
            'product_id' => ['required'],
            'quantity' => ['required'],
            'unit_price' => ['required'],
        ]);

        BatchProduct::create([
            'batch_id' => $request->batch_id,
            'product_id' => $request->product_id,
            'quantity' => $request->quantity,
            'unit_price' => $request->unit_price,
        ]);

        return redirect()->route('admin.batchproducts.index')
            ->with('success', 'Batch Product Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BatchProduct  $batchProduct
     * @return \Illuminate\Http\Response
     */
    public function show(BatchProduct $batchproduct)
    {
        return view('admin.batchproducts.show', compact('batchproduct'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BatchProduct  $batchProduct
     * @return \Illuminate\Http\Response
     */
    public function edit(BatchProduct $batchproduct)
    {
        // $batchProduct = BatchProduct::findOrFail($id);

        $batches = Batch::all();
        $products = Product::all();

        return view('admin.batchproducts.edit', compact('batchproduct', 'batches', 'products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BatchProduct  $batchProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BatchProduct $batchproduct)
    {
        $request->validate([
            'batch_id' => ['required'],
            'product_id' => ['required'],
            'quantity' => ['required'],
            'unit_price' => ['required'],
        ]);


        $batchproduct->update([
            'batch_id' => $request->batch_id,
            'product_id' => $request->product_id,
            'quantity' => $request->quantity,
            'unit_price' => $request->unit_price,
        ]);

        return redirect()->route('admin.batchproducts.index')
            ->with('success', 'Batch Product Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BatchProduct  $batchProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(BatchProduct $batchproduct)
    {
        $batchproduct->delete();

        return redirect()->route('admin.batchproducts.index')
            ->with('success', 'Batch Product Deleted Successfully!');
    }
}

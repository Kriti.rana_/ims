<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use App\Models\Variant;
use Illuminate\Http\Request;
use App\Services\MediaService;
use App\Http\Controllers\Controller;

class VariantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $variants = Variant::simplePaginate(5);

        return view('admin.variants.index', compact('variants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::all();
        return view('admin.variants.create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'product_id' => ['required'],
            'name' => ['required'],
            'price' => ['required'],
            'quantity' => ['required'],
            'details' => ['required'],
            'image' => ['nullable', 'image', 'mimes:png,jpeg,gif'],
        ]);

        if ($request->hasFile('image')) {
            $media_id = MediaService::upload($request->file('image'), "variants");
        }

        Variant::create([
            'product_id' => $request->product_id,
            'name' => $request->name,
            'price' => $request->price,
            'quantity' => $request->quantity,
            'details' => $request->details,
            'media_id' => $media_id ?? null,
        ]);

        return redirect()->route('admin.variants.index')
            ->with('success', 'Variant Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Variant  $variant
     * @return \Illuminate\Http\Response
     */
    public function show(Variant $variant)
    {
        return view('admin.variants.show', compact('variant'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Variant  $variant
     * @return \Illuminate\Http\Response
     */
    public function edit(Variant $variant)
    {
        $products = Product::all();
        return view('admin.variants.edit', compact('variant', 'products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Variant  $variant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Variant $variant)
    {
        $request->validate([
            'product_id' => ['required'],
            'name' => ['required'],
            'price' => ['required'],
            'quantity' => ['required'],
            'details' => ['required'],
        ]);

        $variant->update([
            'product_id' => $request->product_id,
            'name' => $request->name,
            'price' => $request->price,
            'quantity' => $request->quantity,
            'details' => $request->details,
        ]);

        return redirect()->route('admin.variants.index')
            ->with('success', 'Variant Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Variant  $variant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Variant $variant)
    {
        $variant->delete();

        return redirect()->route('admin.variants.index')
            ->with('success', 'Variant Deleted Successfully!');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Purchase;

class ReportController extends Controller
{
    public function index()
    {
        return view('admin.reports.index');
    }

    public function view(Request $request)
    {
        $request->validate([
            'start_date' => ['required', 'date'],
            'end_date' => ['required', 'date', 'after:start_date'],
        ]);

        $reports = [];

        $start_date = $request->date('start_date')->startOfDay();
        $end_date = $request->date('end_date')->endOfDay();

        $orders = Order::whereBetween('created_at', [$start_date, $end_date])->get();
        $purchases = Purchase::whereBetween('created_at', [$start_date, $end_date])->get();

        foreach ($orders as $order) {
            $reports[] = [
                'title' => 'Order from ' . $order->customer->name . ' #' . $order->id,
                'amount' => $order->total,
                'type' => 'Incoming',
                'time' => strtotime($order->created_at->format('Y-m-d')),
            ];
        }

        foreach ($purchases as $purchase) {
            $reports[] = [
                'title' => 'Purchase from ' . $purchase->supplier->name . ' #' . $purchase->id,
                'amount' => $purchase->price * $purchase->quantity,
                'type' => 'Outgoing',
                'time' => strtotime($purchase->created_at->format('Y-m-d')),
            ];
        }


        $reports = collect($reports)->sortBy('time')->toArray();

        return view('admin.reports.view', compact('reports', 'start_date', 'end_date'));
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Stock;
use App\Models\Product;
use App\Models\Purchase;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Notifications\YourStatusUpdateNotification;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $purchases = Purchase::query()
            ->whereNotNull('approved_at')
            ->simplePaginate(5);

        return view('admin.purchases.index', compact('purchases'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::all();
        $suppliers = Supplier::all();

        return view('admin.purchases.create', compact('products', 'suppliers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'product_id' => ['required', 'exists:products,id'],
            'supplier_id' => ['required', 'exists:suppliers,id'],
            'quantity'  => ['required', 'integer', 'gt:0'],
            'price' => ['required', 'integer', 'gt:0'],
        ]);

        DB::transaction(function () use ($data) {
            $data['approved_at'] = now();

            Purchase::create($data);
            $supplier = Supplier::find($data['supplier_id']);

            Stock::create([
                'product_id' => $data['product_id'],
                'quantity' => $data['quantity'],
                'remarks' => 'Purchase from: ' . $supplier->name,
            ]);
        });

        $purchase = User::find($request->user_id);
        $purchase->notify(new YourStatusUpdateNotification);


        return redirect()->route('admin.purchases.index')
            ->with('success', 'New purchase has been recorded successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function show(Purchase $purchase)
    {
        return view('admin.purchases.show', compact('purchase'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase $purchase)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purchase $purchase)
    {
        DB::transaction(function () use ($purchase) {

            // Remove Stock when removing purchase
            Stock::create([
                'product_id' => $purchase->product_id,
                'quantity' => $purchase->quantity * -1,
                'remarks' => 'Delete Purchase from: ' . $purchase->supplier->name,
            ]);

            $purchase->delete();
        });

        return redirect()->route('admin.purchases.index')
            ->with('success', 'Purchase history has been deleted successfully!');
    }

    public function supplies()
    {
        $purchases = Purchase::query()
            ->whereNull('approved_at')
            ->simplePaginate(5);

        return view('admin.purchases.supplies', compact('purchases'));
    }

    public function approve(Purchase $purchase)
    {
        if (!empty($purchase->approved_at)) {
            return redirect()->route('admin.purchases.index')
                ->with('error', 'This product supply request has already been approved!');
        }

        DB::transaction(function () use ($purchase) {
            $purchase->update(['approved_at' => now()]);
            $supplier = Supplier::find($purchase['supplier_id']);

            Stock::create([
                'product_id' => $purchase['product_id'],
                'quantity' => $purchase['quantity'],
                'remarks' => 'Product supplied from: ' . $supplier->name,
            ]);
        });

        return redirect()->route('admin.purchases.index')
            ->with('success', 'Purchase supply request has been approved successfully!');
    }

    public function decline(Purchase $purchase)
    {
        if (!empty($purchase->approved_at)) {
            return redirect()->route('admin.purchases.index')
                ->with('error', 'This product supply request has already been approved!');
        }

        return redirect()->route('admin.purchases.index')
            ->with('success', 'Purchase supply request has been decline successfully!');
    }
}

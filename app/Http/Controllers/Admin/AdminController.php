<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Supplier;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        $count = [
            'category' => Category::count(),
            'customer' => Customer::count(),
            'supplier' => Supplier::count(),

            'product' => Product::count(),
        ];

        return view('admin.index', compact('count'));
    }
}

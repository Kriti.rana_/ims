<?php

namespace App\Http\Controllers\Admin;

use App\Models\Batch;
use App\Models\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BatchProduct;
use App\Models\Product;
use App\Models\Stock;

class BatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $batches = Batch::simplePaginate(5);

        return view('admin.batches.index', compact('batches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $suppliers = Supplier::all();

        return view('admin.batches.create', compact('suppliers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'alpha'],
            'supplier_id' => ['required'],
        ]);

        $batch = Batch::create([
            'name' => $request->name,
            'supplier_id' => $request->supplier_id,
        ]);

        return redirect()->route('admin.batches.products.index', $batch);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Batch  $batch
     * @return \Illuminate\Http\Response
     */
    public function show(Batch $batch)
    {
        return view('admin.batches.show', compact('batch'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Batch  $batch
     * @return \Illuminate\Http\Response
     */
    public function edit(Batch $batch)
    {
        $suppliers = Supplier::all();
        return view('admin.batches.edit', compact('batch', 'suppliers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Batch  $batch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Batch $batch)
    {
        $request->validate([
            'name' => ['required', 'alpha'],
            'supplier_id' => ['required'],
        ]);


        $batch->update([
            'name' => $request->name,
            'supplier_id' => $request->supplier_id,
        ]);

        return redirect()->route('admin.batches.index')
            ->with('success', 'Batch Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Batch  $batch
     * @return \Illuminate\Http\Response
     */
    public function destroy(Batch $batch)
    {
        $batch->delete();

        return redirect()->route('admin.batches.index')
            ->with('success', 'Batch Deleted Successfully!');
    }

    public function products(Batch $batch)
    {
        $products = Product::all();
        $batch_products = BatchProduct::where('batch_id', $batch->id)->get();

        return view('admin.batches.products.index', compact('batch', 'batch_products', 'products'));
    }

    public function addProduct(Request $request, Batch $batch)
    {
        $data = $request->validate([
            'product_id' => ['required', 'exists:products,id'],
            'unit_price' => ['required', 'integer', 'gt:0'],
            'quantity' => ['required', 'integer', 'gt:0'],
        ]);

        $data['batch_id'] = $batch->id;

        BatchProduct::create($data);
        Stock::create([
            'product_id' => $request->product_id,
            'quantity' => $request->quantity,
        ]);

        return redirect()->route('admin.batches.products.index', $batch)
            ->with('success', 'Product added to batch!');
    }

    public function deleteProduct(Batch $batch, BatchProduct $batchproduct)
    {
        Stock::create([
            'product_id' => $batchproduct->product_id,
            'quantity' => $batchproduct->quantity * -1,
        ]);

        $batchproduct->delete();

        return redirect()->route('admin.batches.products.index', $batch)
            ->with('success', 'Product deleted to batch!');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Models\Product;
use App\Models\Customer;
use App\Models\ProductOrder;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Models\Stock;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $orders = Order::simplePaginate(5);

        return view('admin.orders.index', Compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customer::all();
        $products = Product::all();

        return view('admin.orders.create', compact('customers', 'products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'customer_id' => ['required', 'exists:customers,id'],
            'product_id' => ['required', 'exists:products,id'],
            'quantity' => ['required', 'integer', 'min:1'],
        ]);

        $order = Order::create([
            'customer_id' => $request->customer_id
        ]);

        $product = Product::find($request->product_id);

        if (!$this->checkStock($product, $request->quantity)) {
            $product->loadSum('stocks', 'quantity');
            $stock = $product->stocks_sum_quantity;

            return redirect()->route('admin.orders.create')
                ->with('error', 'This product do not have ' . $request->quantity . ' stock! There are currently: ' . $stock . ' left!');
        }

        ProductOrder::create([
            'order_id' => $order->id,
            'product_id' => $product->id,
            'quantity' => $request->quantity,
            'unit_price' => $product->selling_price,
        ]);

        $this->changeStock($product->id, $request->quantity * -1);

        return redirect()->route('admin.orders.show', $order);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $product_orders = ProductOrder::where('order_id', $order->id)
            ->with(['product'])
            ->get();
        $products = Product::all();

        return view('admin.orders.show', compact('order', 'product_orders', 'products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }

    private function checkStock(Product $product, $quantity, $current = 0)
    {
        $product->loadSum('stocks', 'quantity');

        if ($quantity > ($product->stocks_sum_quantity + $current)) {
            return false;
        }

        return true;
    }

    public function addProduct(Request $request, Order $order)
    {
        if (!$order->can_update) {
            return redirect()->route('admin.orders.show', $order)
                ->with('error', 'This order has been marked as ' . $order->status . '! No further modification could be made!');
        }

        $request->validate([
            'product_id' => ['required', 'exists:products,id'],
            'quantity' => ['required', 'integer', 'min:1'],
        ]);

        $product = Product::find($request->product_id);

        // Check if book is already added to order or not
        $check = ProductOrder::where('order_id', $order->id)
            ->where('product_id', $product->id)
            ->first();

        // Check Stock
        if (!$this->checkStock($product, $request->quantity)) {
            $product->loadSum('stocks', 'quantity');
            $stock = $product->stocks_sum_quantity;

            return redirect()->route('admin.orders.show', $order)
                ->with('error', 'This product do not have ' . $request->quantity . ' stock! There are currently: ' . $stock . ' left!');
        }

        if ($check) {

            // If found, add quantity instead of adding another item to order
            $check->update(['quantity' => $check->quantity + $request->quantity]);
            $this->changeStock($product->id, $request->quantity * -1);


            return redirect()->route('admin.orders.show', $order)
                ->with('success', $request->quantity . ' quantity have been added to the order of product: ' . $product->name . '!');
        }

        // If not, proceed as planned to add item to order
        ProductOrder::create([
            'order_id' => $order->id,
            'product_id' => $product->id,
            'quantity' => $request->quantity,
            'unit_price' => $product->selling_price,
        ]);

        $this->changeStock($product->id, $request->quantity * -1);


        return redirect()->route('admin.orders.show', $order)
            ->with('success', 'products has been added to the order!');
    }

    public function editQuantity(Order $order, ProductOrder $product_order)
    {
        if (!$order->can_update) {
            return redirect()->route('admin.orders.show', $order)
                ->with('error', 'This order has been marked as ' . $order->status . '! No further modification could be made!');
        }

        return view('admin.orders.quantity', compact('order', 'product_order'));
    }

    public function updateQuantity(Request $request, Order $order, ProductOrder $product_order)
    {
        if (!$order->can_update) {
            return redirect()->route('admin.orders.show', $order)
                ->with('error', 'This order has been marked as ' . $order->status . '! No further modification could be made!');
        }


        $request->validate([
            'quantity' => ['required', 'integer', 'min:1'],
        ]);

        $product = $product_order->product;

        // Check Stock
        if (!$this->checkStock($product, $request->quantity, $product_order->quantity)) {
            $product->loadSum('stocks', 'quantity');
            $stock = $product->stocks_sum_quantity + $product_order->quantity;

            return redirect()->route('admin.orders.show', $order)
                ->with('error', 'This product do not have ' . $request->quantity . ' stock! There are currently: ' . $stock . ' left!');
        }

        Stock::create([
            'product_id' => $product->id,
            'quantity' => $product_order->quantity,
        ]);

        Stock::create([
            'product_id' => $product->id,
            'quantity' => $request->quantity * -1,
        ]);

        $product_order->update(['quantity' => $request->quantity]);

        return redirect()->route('admin.orders.show', $order)
            ->with('success', 'Product quantity has been updated!');
    }

    public function deleteProduct(Request $request, Order $order, ProductOrder $product_order)
    {
        if (!$order->can_update) {
            return redirect()->route('admin.orders.show', $order)
                ->with('error', 'This order has been marked as ' . $order->status . '! No further modification could be made!');
        }

        $this->changeStock($product_order->product_id, $product_order->quantity);

        $product_order->delete();

        return redirect()->route('admin.orders.show', $order)
            ->with('success', 'Product has been removed from the order!');
    }

    public function changeStatus(Request $request, Order $order)
    {
        $request->validate([
            'status' => ['required', Rule::in(Order::STATUS)],
        ]);

        $order->update(['status' => $request->status]);

        return redirect()->route('admin.orders.show', $order)
            ->with('success', 'Order status has been updated to: ' . $request->status);
    }

    private function changeStock($product_id, $quantity)
    {
        Stock::create([
            'product_id' => $product_id,
            'quantity' => $quantity,
        ]);
    }
}

<?php

namespace App\Http\Controllers\Supplier;


use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SupplierController extends Controller
{
    public function index()
    {
        $count = [
            'product' => Product::where('supplier_id', auth()->user()->supplier->id)->count(),
        ];

        return view('supplier.index', compact('count'));
    }
}

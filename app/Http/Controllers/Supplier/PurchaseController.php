<?php

namespace App\Http\Controllers\Supplier;

use App\Models\User;
use App\Models\Stock;
use App\Models\Product;
use App\Models\Purchase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Notifications\NewProductSupplyRequestNotification;
use App\Notifications\YourProductPlacedSucessfullNotification;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $purchases = Purchase::query()
            ->where('supplier_id', auth()->user()->supplier->id)
            ->simplePaginate(5);

        return view('supplier.purchases.index', compact('purchases'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::query()
            ->where('supplier_id', auth()->user()->supplier->id)
            ->get();

        return view('supplier.purchases.create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'product_id' => ['required', 'exists:products,id'],
            'quantity'  => ['required', 'integer', 'gt:0'],
            'price' => ['required', 'integer', 'gt:0'],
        ]);

        $data['supplier_id'] = auth()->user()->supplier->id;

        $purchase = Purchase::create($data);
        auth()->user()->notify(new YourProductPlacedSucessfullNotification($purchase->id));
        $users = User::where('role', 'admin')->get();
        foreach ($users as $user) {
            $user->notify(new NewProductSupplyRequestNotification());
        }

        return redirect()->route('supplier.purchases.index')
            ->with('success', 'New product supply has been recorded successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function show(Purchase $purchase)
    {
        return view('supplier.purchases.show', compact('purchase'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase $purchase)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purchase $purchase)
    {
        if (!empty($purchase->approved_at)) {
            return redirect()->route('supplier.purchases.index')
                ->with('error', 'You cannot delete already approved record!');
        }

        return redirect()->route('supplier.purchases.index')
            ->with('success', 'Supply history has been deleted successfully!');
    }
}

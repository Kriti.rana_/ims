<?php

namespace App\Http\Controllers\Supplier;

use App\Models\Stock;
use App\Models\Product;
use App\Models\Variant;
use App\Models\Category;
use App\Models\Supplier;
use Illuminate\Http\Request;
use App\Services\MediaService;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::withSum('stocks', 'quantity')
            ->where('supplier_id', auth()->user()->supplier->id)
            ->simplePaginate(10);

        return view('supplier.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $suppliers = Supplier::all();

        return view('supplier.products.create', compact('categories', 'suppliers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required'],
            'category_id' => ['required'],
            'code' => ['required'],
            'buying_price' => ['required'],
            'selling_price' => ['required'],
            'quantity' => ['required'],
            'buying_date' => ['required', 'date'],
            'image' => ['nullable', 'image', 'mimes:png,jpeg,gif'],
        ]);


        if ($request->hasFile('image')) {
            $media_id = MediaService::upload($request->file('image'), "products");
        }

        $product = Product::create([
            'name' => $request->name,
            'category_id' => $request->category_id,
            'supplier_id' => auth()->user()->supplier->id,
            'code' => $request->code,
            'buying_price' => $request->buying_price,
            'selling_price' => $request->selling_price,
            'quantity' => $request->quantity,
            'buying_date' => Carbon::parse($request->buying_date)->format('Y-m-d'),
            'media_id' => $media_id ?? null,
        ]);

        Stock::create([
            'product_id' => $product->id,
            'quantity' => $request->quantity,
        ]);



        return redirect()->route('supplier.products.index')
            ->with('success', 'Product Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('supplier.products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Category::all();
        $suppliers = Supplier::all();
        return view('supplier.products.edit', compact('product', 'categories', 'suppliers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
            'name' => ['required'],
            'category_id' => ['required'],
            'code' => ['required'],
            'buying_price' => ['required'],
            'selling_price' => ['required'],
            'quantity' => ['required'],
            'buying_date' => ['required', 'date'],

        ]);


        $product->update([
            'name' => $request->name,
            'category_id' => $request->category_id,
            'code' => $request->code,
            'buying_price' => $request->buying_price,
            'selling_price' => $request->selling_price,
            'quantity' => $request->quantity,
            'buying_date' => Carbon::parse($request->buying_date)->format('Y-m-d'),

        ]);

        return redirect()->route('supplier.products.index')
            ->with('success', 'Product Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('supplier.products.index')
            ->with('success', 'Product Deleted Successfully!');
    }

    public function search(Request $request)
    {
        if (empty($request->q)) {
            return redirect()->route('supplier.products.index');
        }

        $search = trim(strip_tags($request->q));

        $products = Product::where('name', 'LIKE', '%' . $search . '%')
            ->withSum('stocks', 'quantity')
            ->get();

        return view('supplier.products.search', compact('products', 'search'));
    }

    public function variants(Product $product)
    {
        $products = Variant::where('product_id', $product->id)
            ->orWhere('variant_id', $product->id)
            ->get();

        $all_products = Product::where('id', '!=', $product->id)->get();

        return view('supplier.products.variants', compact('products', 'all_products', 'product'));
    }

    public function addVariant(Request $request, Product $product)
    {
        $request->validate([
            'product_id' => ['required', 'exists:products,id'],
        ]);

        // Check if already exists
        $check1 = Variant::where([
            'product_id' => $product->id,
            'variant_id' => $request->product_id,
        ])->count();

        $check2 = Variant::where([
            'product_id' => $request->product_id,
            'variant_id' => $product->id,
        ])->count();

        if ($check1 != 0 || $check2 != 0) {
            return redirect()->route('supplier.products.variants', $product)
                ->with('error', 'Duplicate entry found! This product is already an variant!');
        }

        Variant::create([
            'product_id' => $product->id,
            'variant_id' => $request->product_id,
        ]);

        return redirect()->route('supplier.products.variants', $product)
            ->with('success', 'Variant added successfully!');
    }

    public function removeVariant(Product $product, Variant $variant)
    {
        $variant->delete();

        return redirect()->route('supplier.products.variants', $product)
            ->with('success', 'Variant has been removed successfully!');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'user_id', 'address', 'phone', 'shop_name', 'media_id'
    ];

    public function media()
    {
        return $this->belongsTo(Media::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function batches()
    {
        return $this->hasMany(Batch::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function purchases()
    {
        return $this->hasMany(Purchase::class);
    }
}

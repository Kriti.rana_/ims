<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $fillable = ['customer_id', 'status'];

    const STATUS = [
        'Pending', 'Confirmed', 'Delivering', 'Delivered', 'Complete', 'Canceled', 'Refunded',
    ];

    public function getCanUpdateAttribute()
    {
        return !in_array($this->status, ['Canceled', 'Refunded', 'Complete']);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function products()
    {
        return $this->belongsToMany(Order::class)->withPivot(['quantity', 'unit_price']);
    }

    public function getTotalAttribute()
    {
        $total = 0;
        $ops = ProductOrder::where('order_id', $this->id)->get();
        foreach ($ops as $op) {
            $total += $op->unit_price * $op->quantity;
        }

        return $total;
    }
}

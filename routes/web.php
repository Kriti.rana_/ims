<?php

use App\Http\Controllers\Admin;
use App\Http\Controllers\Supplier;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\POSController;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\StockController;
use App\Http\Controllers\Admin\PurchaseController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [SiteController::class, 'index'])->name('index');
Route::get('/home', [SiteController::class, 'home'])
    ->name('home')
    ->middleware('auth');
Route::get('/404', [SiteController::class, 'notFound'])->name('404');

Route::controller(POSController::class)
    ->middleware('cashier')
    ->prefix('/pos')
    ->name('pos.')
    ->group(function () {
        Route::get('/', 'index')->name('index');
        Route::post('/select-customer', 'selectCustomer')->name('customer.select');
        Route::post('/new-customer', 'createCustomer')->name('customer.create');
        Route::post('/no-customer', 'emptyCustomer')->name('customer.none');

        Route::get('/order/{order}', 'order')->name('order');
        Route::post('/order/{order}/status', 'changeStatus')->name('status');
        Route::post('/order/{order}/products', 'addProduct')->name('products.store');
        Route::get('/order/{order}/{product_order}/delete', 'deleteProduct')->name('products.delete');
        Route::post('/order/{order}/quantity', 'updateQuantity')->name('quantity');
    });

Route::middleware(['auth', 'admin'])->prefix('/admin')->name('admin.')->group(function () {
    Route::get('/', [Admin\AdminController::class, 'index'])->name('index');

    //CRUD
    Route::resource('categories', Admin\CategoryController::class);
    Route::resource('suppliers', Admin\SupplierController::class);
    Route::resource('customers', Admin\CustomerController::class);

    Route::controller(Admin\ProductController::class)->name('products.')->group(function () {
        Route::get('products/{product}/variants', 'variants')->name('variants');
        Route::post('products/{product}/variants', 'addVariant')->name('variants');
        Route::delete('products/{product}/variants/{variant}', 'removeVariant')->name('variants.delete');
        Route::get('products/search', 'search')->name('search');
    });

    Route::resource('products', Admin\ProductController::class);
    Route::get('batches/{batch}/products', [Admin\BatchController::class, 'products'])->name('batches.products.index');
    Route::post('batches/{batch}/products', [Admin\BatchController::class, 'addProduct'])->name('batches.products.store');
    Route::delete('batches/{batch}/products/{batchproduct}', [Admin\BatchController::class, 'deleteProduct'])->name('batches.products.destory');
    Route::resource('batches', Admin\BatchController::class);
    Route::resource('batchproducts', Admin\BatchProductController::class);
    Route::resource('stocks', Admin\StockController::class);

    Route::get('purchases/supplies', [Admin\PurchaseController::class, 'supplies'])->name('purchases.supplies.index');
    Route::get('purchases/{purchase}/approve', [Admin\PurchaseController::class, 'approve'])->name('purchases.supplies.approve');
    Route::delete('purchases/{purchase}/decline', [Admin\PurchaseController::class, 'decline'])->name('purchases.supplies.decline');
    Route::resource('purchases', Admin\PurchaseController::class);

    Route::resource('users', Admin\UserController::class);
    Route::prefix('orders')->name('orders.')->controller(Admin\OrderController::class)->group(function () {
        Route::post('{order}/status', 'changeStatus')->name('status');
        Route::post('{order}/products', 'addProduct')->name('products.store');
        Route::get('{order}/{product_order}/quantity', 'editQuantity')->name('products.quantity');
        Route::post('{order}/{product_order}/quantity', 'updateQuantity');
        Route::get('{order}/{product_order}/delete', 'deleteProduct')->name('products.delete');
    });
    Route::resource('orders', Admin\OrderController::class);

    Route::get('/reports', [Admin\ReportController::class, 'index'])->name('reports.index');
    Route::get('/reports/view', [Admin\ReportController::class, 'view'])->name('reports.view');
});


Route::middleware(['auth', 'supplier'])->prefix('/supplier')->name('supplier.')->group(function () {
    Route::get('/', [Supplier\SupplierController::class, 'index'])->name('index');


    //CRUD
    Route::controller(Supplier\ProductController::class)->name('products.')->group(function () {
        Route::get('products/{product}/variants', 'variants')->name('variants');
        Route::post('products/{product}/variants', 'addVariant')->name('variants');
        Route::delete('products/{product}/variants/{variant}', 'removeVariant')->name('variants.delete');
        Route::get('products/search', 'search')->name('search');
    });
    Route::resource('products', Supplier\ProductController::class);
    Route::resource('purchases', Supplier\PurchaseController::class);
    Route::resource('suppliers', SupplierController::class);
    Route::resource('variants', \App\Http\Controllers\Supplier\ProductController::class);
    // Route::resource('customers', CustomerController::class);
    // Route::resource('products', ProductController::class);
    // Route::resource('variants', VariantController::class);
});
